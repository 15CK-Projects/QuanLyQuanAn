﻿CREATE TABLE [dbo].[ChiTietDonHang]
(
	DonHangId						int,
	MonAnId							int,
	SoLuong							decimal(15,4),
	DonGia							decimal(15,4),
	ThanhTien						decimal(15,4),

	CONSTRAINT PK_ChiTietDonHang PRIMARY KEY (DonHangId, MonAnId)
);
