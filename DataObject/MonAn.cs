﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanAn.DataObject
{
    public class MonAn
    {
        private int? _id;
        private string _tenMonAn;
        private int? _danhMucId;
        private string _moTa;
        private string _hinhAnh;
        private decimal? _giaThamKhao;
        private bool? _khongThongKe;
        private DateTime? _thoiGianTao;
        private DateTime? _thoiGianCapNhat;

        public int? Id { get => _id; set => _id = value; }
        public string TenMonAn { get => _tenMonAn; set => _tenMonAn = value; }
        public int? DanhMucId { get => _danhMucId; set => _danhMucId = value; }
        public string MoTa { get => _moTa; set => _moTa = value; }
        public string HinhAnh { get => _hinhAnh; set => _hinhAnh = value; }
        public decimal? GiaThamKhao { get => _giaThamKhao; set => _giaThamKhao = value; }
        public bool? KhongThongKe { get => _khongThongKe; set => _khongThongKe = value; }
        public DateTime? ThoiGianTao { get => _thoiGianTao; set => _thoiGianTao = value; }
        public DateTime? ThoiGianCapNhat { get => _thoiGianCapNhat; set => _thoiGianCapNhat = value; }
    }
}
