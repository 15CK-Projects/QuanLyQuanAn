﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanAn.GUI.BoPhanTrucTongDaiGUI
{
    public class DonHang
    {
        int _NhanVien;

        int _KhachHang;

        DateTime _ThoiGianTao;

        int _ChiNhanh;

        int _BanChiNhanh;

        Decimal _TongTien;

        int _TrangThai;

        public int NhanVien { get => _NhanVien; set => _NhanVien = value; }
        public int KhachHang { get => _KhachHang; set => _KhachHang = value; }
        public DateTime ThoiGianTao { get => _ThoiGianTao; set => _ThoiGianTao = value; }
        public int ChiNhanh { get => _ChiNhanh; set => _ChiNhanh = value; }
        public int BanChiNhanh { get => _BanChiNhanh; set => _BanChiNhanh = value; }
        public decimal TongTien { get => _TongTien; set => _TongTien = value; }
        public int TrangThai { get => _TrangThai; set => _TrangThai = value; }
    }
}
