﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;
using QuanLyQuanAn.DataAccessLayer;

namespace QuanLyQuanAn.Service
{
    public class ChiNhanhService : IChiNhanhService
    {
        public bool AddBanChiNhanh(BanChiNhanh banChiNhanh)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                bool isOk = uow.ChiNhanhRepository.InsertBanChiNhanh(banChiNhanh);
                if (isOk)
                    uow.Commit();
                return isOk;
            }
        }

        public bool AddChiNhanh(ChiNhanh chiNhanh)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                bool isOk = uow.ChiNhanhRepository.InsertChiNhanh(chiNhanh);
                if (isOk)
                    uow.Commit();
                return isOk;
            }
        }

        public bool AddMonAnChiNhanh(MonAnChiNhanh monAnChiNhanh)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                bool isOk = uow.ChiNhanhRepository.InsertMonAnChiNhanh(monAnChiNhanh);
                if (isOk)
                    uow.Commit();
                return isOk;
            }
        }

        public bool DeleteAllBanChiNhanh(int chiNhanhId)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                var listBanChiNhanh = uow.ChiNhanhRepository.FindBanByChiNhanhId(chiNhanhId);
                foreach (BanChiNhanh banChiNhanh in listBanChiNhanh)
                {
                    if (!uow.ChiNhanhRepository.DeleteBanChiNhanh(banChiNhanh.Id.Value))
                    {
                        return false;
                    }
                }
                uow.Commit();
                return true;
            }
        }

        public bool DeleteAllMonAnChiNhanh(int chiNhanhId)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                var listMonAnChiNhanh = uow.ChiNhanhRepository.FindMonAnChiNhanhByChiNhanhId(chiNhanhId);
                foreach (MonAnChiNhanh monAnChiNhanh in listMonAnChiNhanh)
                {
                    if (!uow.ChiNhanhRepository.DeleteMonAnChiNhanh(monAnChiNhanh))
                    {
                        return false;
                    }
                }
                uow.Commit();
                return true;
            }
        }

        public bool DeleteBanChiNhanh(int id)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                bool isOk = uow.ChiNhanhRepository.DeleteBanChiNhanh(id);
                if (isOk)
                    uow.Commit();
                return isOk;
            }
        }

        public bool DeleteChiNhanh(int id)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                IEnumerable<BanChiNhanh> listBan = FindBanByChiNhanhId(id);
                IEnumerable<MonAnChiNhanh> menuChiNhanh = FindMonAnChiNhanhByChiNhanhId(id);
                foreach (BanChiNhanh banChiNhanh in listBan)
                    if (!uow.ChiNhanhRepository.DeleteBanChiNhanh(banChiNhanh.Id.Value))
                        return false;
                foreach (MonAnChiNhanh item in menuChiNhanh)
                    if (!uow.ChiNhanhRepository.DeleteMonAnChiNhanh(item))
                        return false;
                bool isOk = uow.ChiNhanhRepository.DeleteChiNhanh(id);
                if (isOk)
                    uow.Commit();
                return isOk;
            }
        }

        public bool DeleteMonAnChiNhanh(MonAnChiNhanh monAnChiNhanh)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                bool isOk = uow.ChiNhanhRepository.DeleteMonAnChiNhanh(monAnChiNhanh);
                if (isOk)
                    uow.Commit();
                return isOk;
            }
        }

        public bool DuplicateBanChiNhanh(int chiNhanhIdFrom, int chiNhanhIdTo)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                // Clear all BanChiNhanh.
                var listBanChiNhanhOld = uow.ChiNhanhRepository.FindBanByChiNhanhId(chiNhanhIdTo);
                foreach (BanChiNhanh banChiNhanh in listBanChiNhanhOld)
                {
                    if (!uow.ChiNhanhRepository.DeleteBanChiNhanh(banChiNhanh.Id.Value))
                    {
                        return false;
                    }
                }

                // Duplicate.
                var listBanChiNhanhNew = uow.ChiNhanhRepository.FindBanByChiNhanhId(chiNhanhIdFrom);
                foreach (BanChiNhanh banChiNhanh in listBanChiNhanhNew)
                {
                    banChiNhanh.ChiNhanhId = chiNhanhIdTo;
                    if (!uow.ChiNhanhRepository.InsertBanChiNhanh(banChiNhanh))
                        return false;
                }

                uow.Commit();
                return true;
            }
        }

        public bool DuplicateMonAnChiNhanh(int chiNhanhIdFrom, int chiNhanhIdTo)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                // Clear all MonAnChiNhanh
                var listMonAnChiNhanhOld = uow.ChiNhanhRepository.FindMonAnChiNhanhByChiNhanhId(chiNhanhIdTo);
                foreach (MonAnChiNhanh monAnChiNhanh in listMonAnChiNhanhOld)
                {
                    if (!uow.ChiNhanhRepository.DeleteMonAnChiNhanh(monAnChiNhanh))
                        return false;
                }

                // Duplicate.
                var listMonAnChiNhanhNew = uow.ChiNhanhRepository.FindMonAnChiNhanhByChiNhanhId(chiNhanhIdFrom);
                foreach (MonAnChiNhanh monAnChiNhanh in listMonAnChiNhanhNew)
                {
                    monAnChiNhanh.ChiNhanhId = chiNhanhIdTo;
                    if (!uow.ChiNhanhRepository.InsertMonAnChiNhanh(monAnChiNhanh))
                        return false;
                }

                uow.Commit();
                return true;
            }
                
        }

        public IEnumerable<BanChiNhanh> FindBanByChiNhanhId(int chiNhanhId)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                return uow.ChiNhanhRepository.FindBanByChiNhanhId(chiNhanhId);
            }
        }

        public IEnumerable<MonAnChiNhanh> FindMonAnChiNhanhByChiNhanhId(int chiNhanhId)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                return uow.ChiNhanhRepository.FindMonAnChiNhanhByChiNhanhId(chiNhanhId);
            }
        }

        public IEnumerable<ChiNhanh> GetAllChiNhanh()
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                return uow.ChiNhanhRepository.GetAllChiNhanh();
            }
        }

        public ChiNhanh GetChiNhanh(int id)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                return uow.ChiNhanhRepository.GetChiNhanh(id);
            }
        }

        public bool UpdateBanChiNhanh(BanChiNhanh banChiNhanh)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                bool isOk = uow.ChiNhanhRepository.UpdateBanChiNhanh(banChiNhanh);
                if (isOk)
                    uow.Commit();
                return isOk;
            }
        }

        public bool UpdateChiNhanh(ChiNhanh chiNhanh)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                bool isOk = uow.ChiNhanhRepository.UpdateChiNhanh(chiNhanh);
                if (isOk)
                    uow.Commit();
                return isOk;
            }
        }

        public bool UpdateMonAnChiNhanh(MonAnChiNhanh monAnChiNhanh)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                bool isOk = uow.ChiNhanhRepository.UpdateMonAnChiNhanh(monAnChiNhanh);
                if (isOk)
                    uow.Commit();
                return isOk;
            }
        }
    }
}
