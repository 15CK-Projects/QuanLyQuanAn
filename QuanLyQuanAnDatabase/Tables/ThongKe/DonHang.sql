﻿CREATE TABLE [dbo].[DonHang]
(
	Id					int IDENTITY(1,1),
	ThoiGianTao			datetime DEFAULT GETDATE(),
	NhanVienId			int,
	KhachHangId			int,
	ChiNhanhId			int,
	BanChiNhanhId		int,
	TongTien			decimal(15,4),
	TrangThai			int,

	CONSTRAINT PK_DonHang PRIMARY KEY (Id)
);
