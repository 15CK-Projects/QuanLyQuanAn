﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLyQuanAn.GUI.BoPhanQuanLyGUI
{
    /// <summary>
    /// Interaction logic for MessageDialog.xaml
    /// </summary>
    public partial class MessageDialog : Window
    {
        public MessageDialog()
        {
            InitializeComponent();
        }

        private void MessageDialogWindow_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private DialogManager.DialogButton _messageDialogButton;
        public DialogManager.DialogResult _messageDialogDefaultButton;
        public DialogManager.DialogImage _messageDialogImage;
        public DialogManager.DialogResult _messageDialogResult;

        public DialogManager.DialogButton MessageDialogButton
        {
            get
            {
                return _messageDialogButton;
            }
            set
            {
                _messageDialogButton = value;
                PrepareButton();
            }
        }

        public DialogManager.DialogResult MessageDialogDefaultButton
        {
            get
            {
                return _messageDialogDefaultButton;
            }
            set
            {
                _messageDialogDefaultButton = value;
                PrepareDefaultButton();
            }
        }

        public DialogManager.DialogImage MessageDialogImage
        {
            get
            {
                return _messageDialogImage;
            }
            set
            {
                _messageDialogImage = value;
                PrepareImage();
            }
        }
        
        public DialogManager.DialogResult MessageDialogResult
        {
            get
            {
                return _messageDialogResult;
            }
            set
            {
                _messageDialogResult = value;
            }
        }

        public string Message
        {
            get
            {
                return TextBlock_Message.Text;
            }
            set
            {
                TextBlock_Message.Text = value;
            }
        }
        
        private void PrepareImage()
        {
            switch (MessageDialogImage)
            {
                case DialogManager.DialogImage.Asterisk:
                    ViewBox_IconBox.Child = Resources["Icon_Asterisk"] as UIElement;
                    break;
                case DialogManager.DialogImage.Error:
                    ViewBox_IconBox.Child = Resources["Icon_Error"] as UIElement;
                    break;
                case DialogManager.DialogImage.Exclamation:
                    ViewBox_IconBox.Child = Resources["Icon_Exclamation"] as UIElement;
                    break;
                case DialogManager.DialogImage.Hand:
                    ViewBox_IconBox.Child = Resources["Icon_Hand"] as UIElement;
                    break;
                case DialogManager.DialogImage.Information:
                    ViewBox_IconBox.Child = Resources["Icon_Information"] as UIElement;
                    break;
                case DialogManager.DialogImage.None:
                    ViewBox_IconBox.Child = Resources["Icon_None"] as UIElement;
                    // Không hiển thị Icon.
                    break;
                case DialogManager.DialogImage.Question:
                    ViewBox_IconBox.Child = Resources["Icon_Question"] as UIElement;
                    break;
                case DialogManager.DialogImage.Stop:
                    ViewBox_IconBox.Child = Resources["Icon_Stop"] as UIElement;
                    break;
                case DialogManager.DialogImage.Warning:
                    ViewBox_IconBox.Child = Resources["Icon_Warning"] as UIElement;
                    break;
            }
        }

        private void ResetIsDefault()
        {
            Button_Abort.IsDefault = false;
            Button_Cancel.IsDefault = false;
            Button_Ignore.IsDefault = false;
            Button_No.IsDefault = false;
            Button_OK.IsDefault = false;
            Button_Retry.IsDefault = false;
            Button_Yes.IsDefault = false;
        }

        private void ResetIsCancel()
        {
            Button_Abort.IsCancel = false;
            Button_Cancel.IsCancel = false;
            Button_Ignore.IsCancel = false;
            Button_No.IsCancel = false;
            Button_OK.IsCancel = false;
            Button_Retry.IsCancel = false;
            Button_Yes.IsCancel = false;
        }
        
        private void PrepareButton()
        {
            HideAllButton();
            ResetIsDefault();
            ResetIsCancel();
            switch (MessageDialogButton)
            {
                case DialogManager.DialogButton.AbortRetryIgnore:
                    Button_Abort.Visibility = Button_Retry.Visibility = Button_Ignore.Visibility = Visibility.Visible;
                    Button_Retry.IsDefault = true;
                    Button_Ignore.IsCancel = true;
                    break;
                case DialogManager.DialogButton.OK:
                    Button_OK.Visibility = Visibility.Visible;
                    Button_OK.IsDefault = true;
                    break;
                case DialogManager.DialogButton.OKCancel:
                    Button_OK.Visibility = Button_Cancel.Visibility = Visibility.Visible;
                    Button_OK.IsDefault = true;
                    Button_Cancel.IsCancel = true;
                    break;
                case DialogManager.DialogButton.RetryCancel:
                    Button_Retry.Visibility = Button_Cancel.Visibility = Visibility.Visible;
                    Button_Retry.IsDefault = true;
                    Button_Cancel.IsCancel = true;
                    break;
                case DialogManager.DialogButton.YesNo:
                    Button_Yes.Visibility = Button_No.Visibility = Visibility.Visible;
                    Button_Yes.IsDefault = true;
                    Button_No.IsCancel = true;
                    break;
                case DialogManager.DialogButton.YesNoCancel:
                    Button_Yes.Visibility = Button_No.Visibility = Button_Cancel.Visibility = Visibility.Visible;
                    Button_Yes.IsDefault = true;
                    Button_Cancel.IsCancel = true;
                    break;
            }
        }

        private void PrepareDefaultButton()
        {
            ResetIsDefault();
            switch (MessageDialogDefaultButton)
            {
                case DialogManager.DialogResult.Abort:
                    Button_Abort.IsDefault = true;
                    break;
                case DialogManager.DialogResult.Cancel:
                    Button_Cancel.IsDefault = true;
                    break;
                case DialogManager.DialogResult.Ignore:
                    Button_Ignore.IsDefault = true;
                    break;
                case DialogManager.DialogResult.No:
                    Button_No.IsDefault = true;
                    break;
                case DialogManager.DialogResult.OK:
                    Button_OK.IsDefault = true;
                    break;
                case DialogManager.DialogResult.Retry:
                    Button_Retry.IsDefault = true;
                    break;
                case DialogManager.DialogResult.Yes:
                    Button_Yes.IsDefault = true;
                    break;
            }
        }

        private void HideAllButton()
        {
            Button_Abort.Visibility = Visibility.Collapsed;
            Button_Cancel.Visibility = Visibility.Collapsed;
            Button_Ignore.Visibility = Visibility.Collapsed;
            Button_No.Visibility = Visibility.Collapsed;
            Button_OK.Visibility = Visibility.Collapsed;
            Button_Retry.Visibility = Visibility.Collapsed;
            Button_Yes.Visibility = Visibility.Collapsed;
        }

        public MessageDialog SetCaption(string caption)
        {
            Title = caption;
            return this;
        }

        public MessageDialog SetMessage(string message)
        {
            Message = message;
            return this;

        }

        public MessageDialog SetStartupLocation(WindowStartupLocation startupLocation)
        {
            WindowStartupLocation = startupLocation;
            return this;
        }

        public MessageDialog SetButton(DialogManager.DialogButton dialogButton)
        {
            MessageDialogButton = dialogButton;
            return this;
        }

        public MessageDialog SetDefaultButton(DialogManager.DialogResult defaultResult)
        {
            MessageDialogDefaultButton = defaultResult;
            return this;
        }

        public MessageDialog SetImage(DialogManager.DialogImage dialogImage)
        {
            MessageDialogImage = dialogImage;
            return this;
        }

        private void Button_Yes_Click(object sender, RoutedEventArgs e)
        {
            MessageDialogResult = DialogManager.DialogResult.Yes;
            DialogResult = true;
        }

        private void Button_OK_Click(object sender, RoutedEventArgs e)
        {
            MessageDialogResult = DialogManager.DialogResult.OK;
            DialogResult = true;
        }

        private void Button_Abort_Click(object sender, RoutedEventArgs e)
        {
            MessageDialogResult = DialogManager.DialogResult.Abort;
            DialogResult = true;
        }

        private void Button_Retry_Click(object sender, RoutedEventArgs e)
        {
            MessageDialogResult = DialogManager.DialogResult.Retry;
            DialogResult = true;
        }

        private void Button_Ignore_Click(object sender, RoutedEventArgs e)
        {
            MessageDialogResult = DialogManager.DialogResult.Ignore;
            DialogResult = true;
        }

        private void Button_No_Click(object sender, RoutedEventArgs e)
        {
            MessageDialogResult = DialogManager.DialogResult.No;
            DialogResult = false;
        }

        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            MessageDialogResult = DialogManager.DialogResult.Cancel;
            DialogResult = false;
        }
    }
}
