﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;
using QuanLyQuanAn.DataAccessLayer;

namespace QuanLyQuanAn.Service
{
    public class NhomTaiKhoanService : INhomTaiKhoanService
    {
        public bool AddNhomTaiKhoan(NhomTaiKhoan nhom)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                bool result = uow.NhomTaiKhoanRepository.AddNhomTaiKhoan(nhom);
                uow.Commit();
                return result;
            }
        }

        public bool DeleteNhomTaiKhoan(int id)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                bool result = uow.NhomTaiKhoanRepository.DeleteNhomTaiKhoan(id);
                uow.Commit();
                return result;
            }
        }

        public IList<NhomTaiKhoan> GetAllNhomTaiKhoan()
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                return uow.NhomTaiKhoanRepository.GetAll().ToList();
            }
        }

        public bool UpdateNhomTaiKhoan(NhomTaiKhoan nhom)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                bool result = uow.NhomTaiKhoanRepository.UpdateNhomTaiKhoan(nhom);
                uow.Commit();
                return result;
            }
        }

        public NhomTaiKhoan GetNhomTaiKhoan(int id)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                return uow.NhomTaiKhoanRepository.GetNhomTaiKhoan(id);
            }
        }
    }
}
