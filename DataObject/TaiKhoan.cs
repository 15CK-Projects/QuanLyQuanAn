﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanAn.DataObject
{
    public class TaiKhoan
    {
        private int? _id;
        private string _userName;
        private string _password;
        private int? _nhomTaiKhoanId;
        private bool? _isLocked;
        private int? _nhanVienId;
        private DateTime? _thoiGianTao;
        private DateTime? _thoiGianCapNhat;

        public int? Id { get => _id; set => _id = value; }
        public string UserName { get => _userName; set => _userName = value; }
        public string Password { get => _password; set => _password = value; }
        public int? NhomTaiKhoanId { get => _nhomTaiKhoanId; set => _nhomTaiKhoanId = value; }
        public bool? IsLocked { get => _isLocked; set => _isLocked = value; }
        public int? NhanVienId { get => _nhanVienId; set => _nhanVienId = value; }
        public DateTime? ThoiGianTao { get => _thoiGianTao; set => _thoiGianTao = value; }
        public DateTime? ThoiGianCapNhat { get => _thoiGianCapNhat; set => _thoiGianCapNhat = value; }
    }
}
