﻿CREATE TABLE [dbo].[MonAnChiNhanh]
(
	ChiNhanhId				int,
	MonAnId					int,	
	GiaBan					decimal(15,4) DEFAULT 0.0,
	ThoiGianTao				datetime DEFAULT GETDATE(),
	ThoiGianCapNhat			datetime DEFAULT GETDATE(),

	CONSTRAINT PK_MonAnChiNhanh PRIMARY KEY (ChiNhanhId, MonAnId)
);
