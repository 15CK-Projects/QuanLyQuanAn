﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using QuanLyQuanAn.DataObject;
using QuanLyQuanAn.DataAccessLayer;
using QuanLyQuanAn.Common;

namespace QuanLyQuanAn.Service
{
    public class MonAnService : IMonAnService
    {
        public bool DeleteMonAn(int id)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                if (!uow.MonAnRepository.DeleteMonAn(id))
                    return false;
                uow.Commit();
                return true;
            }
        }

        public IEnumerable<MonAn> GetAllMonAn()
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                var listMonAn = uow.MonAnRepository.GetAll();
                uow.Commit();
                foreach (MonAn monAn in listMonAn)
                {
                    var imagePath = monAn.HinhAnh;
                    HandleImagePathWhenLoad(ref imagePath);
                    monAn.HinhAnh = imagePath;

                    monAn.KhongThongKe = uow.MonAnRepository.IsMonAnIdInKhongThongKe(monAn.Id.Value);

                    // Nếu DanhMucId = null => Món ăn không thuộc danh mục nào, điều chỉnh lại DanhMucId theo định nghĩa.
                    if (monAn.DanhMucId == null)
                        monAn.DanhMucId = DanhMucHeThong.ChuaPhanLoai.Id;
                }
                return listMonAn;
            }
        }

        public IEnumerable<MonAn> GetMonAnInDanhMucChuaPhanLoai()
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                var listMonAn = uow.MonAnRepository.GetByDanhMucId(null);
                uow.Commit();
                foreach (MonAn monAn in listMonAn)
                {
                    var imagePath = monAn.HinhAnh;
                    HandleImagePathWhenLoad(ref imagePath);
                    monAn.HinhAnh = imagePath;

                    monAn.KhongThongKe = uow.MonAnRepository.IsMonAnIdInKhongThongKe(monAn.Id.Value);

                    monAn.DanhMucId = DanhMucHeThong.ChuaPhanLoai.Id;
                }
                return listMonAn;
            }
        }

        public IEnumerable<MonAn> GetMonAnByDanhMucId(int danhMucId)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                var listMonAn = uow.MonAnRepository.GetByDanhMucId(danhMucId);
                uow.Commit();
                foreach (MonAn monAn in listMonAn)
                {
                    var imagePath = monAn.HinhAnh;
                    HandleImagePathWhenLoad(ref imagePath);
                    monAn.HinhAnh = imagePath;

                    monAn.KhongThongKe = uow.MonAnRepository.IsMonAnIdInKhongThongKe(monAn.Id.Value);
                }
                return listMonAn;
            }            
        }

        public IEnumerable<MonAn> GetTop10MonAnBanChayTatCaChiNhanhTrongThang()
        {
            DateTime from = new DateTime(year: DateTime.Now.Year, month: DateTime.Now.Month, day: 1, hour: 0, minute: 0, second: 0, millisecond: 0);
            DateTime to = new DateTime(year: DateTime.Now.Year,
                month: DateTime.Now.Month,
                day: DateTime.DaysInMonth(year: DateTime.Now.Year, month: DateTime.Now.Month),
                hour: 23, minute: 59, second: 59, millisecond: 999);

            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                var listMonAn = uow.ThongKeRepository.GetTop10MonAnBanChayTatCaChiNhanh(from, to);
                uow.Commit();
                foreach (MonAn monAn in listMonAn)
                {
                    var imagePath = monAn.HinhAnh;
                    HandleImagePathWhenLoad(ref imagePath);
                    monAn.HinhAnh = imagePath;

                    monAn.KhongThongKe = uow.MonAnRepository.IsMonAnIdInKhongThongKe(monAn.Id.Value);

                    // Nếu DanhMucId = null => Món ăn không thuộc danh mục nào, điều chỉnh lại DanhMucId theo định nghĩa.
                    if (monAn.DanhMucId == null)
                        monAn.DanhMucId = DanhMucHeThong.ChuaPhanLoai.Id;
                }
                return listMonAn;
            }
        }

        public MonAn GetMonAn(int id)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                var monAn = uow.MonAnRepository.GetMonAn(id);
                if (monAn != null)
                {
                    var imagePath = monAn.HinhAnh;
                    HandleImagePathWhenLoad(ref imagePath);
                    monAn.HinhAnh = imagePath;

                    monAn.KhongThongKe = uow.MonAnRepository.IsMonAnIdInKhongThongKe(monAn.Id.Value);

                    // Nếu DanhMucId = null => Món ăn không thuộc danh mục nào, điều chỉnh lại DanhMucId theo định nghĩa.
                    if (monAn.DanhMucId == null)
                        monAn.DanhMucId = DanhMucHeThong.ChuaPhanLoai.Id;
                }
                uow.Commit();
                return monAn;
            }
        }

        public bool InsertMonAn(MonAn monAn)
        {
            var imagePath = monAn.HinhAnh;
            HandleImagePathWhenWrite(ref imagePath);
            monAn.HinhAnh = imagePath;

            if (monAn.DanhMucId == DanhMucHeThong.ChuaPhanLoai.Id)
                monAn.DanhMucId = null;

            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                if (!uow.MonAnRepository.InsertMonAn(monAn))
                    return false;
                if (!uow.MonAnRepository.InsertMonAnIntoKhongThongKe(monAn.Id.Value))
                    return false;
                uow.Commit();
                return true;
            }
        }

        public bool UpdateMonAn(MonAn monAn)
        {
            var imagePath = monAn.HinhAnh;
            HandleImagePathWhenWrite(ref imagePath);
            monAn.HinhAnh = imagePath;
            if (monAn.DanhMucId == DanhMucHeThong.ChuaPhanLoai.Id)
                monAn.DanhMucId = null;

            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                if (!uow.MonAnRepository.UpdateMonAn(monAn))
                    return false;
                bool currentKhongThongKe = uow.MonAnRepository.IsMonAnIdInKhongThongKe(monAn.Id.Value);
                if (currentKhongThongKe == true && monAn.KhongThongKe == false)
                    if (!uow.MonAnRepository.DeleteMonAnIdFromKhongThongKe(monAn.Id.Value))
                        return false;
                if (currentKhongThongKe == false && monAn.KhongThongKe == true)
                    if (!uow.MonAnRepository.InsertMonAnIntoKhongThongKe(monAn.Id.Value))
                        return false;

                uow.Commit();
                return true;
            }
        }

        /// <summary>
        /// Điều chỉnh lại tên file ảnh và lưu trữ file ảnh nếu cần thiết.
        /// Chỉ trả về tên file.
        /// </summary>
        /// <param name="inputImagePath"></param>
        private void HandleImagePathWhenWrite(ref string inputImagePath)
        {
            // File ảnh: No-Image.png là file ảnh mặc định. Luôn luôn tồn tại file này trong thư mục Image.

            // Nếu đường dẫn null (hoặc trống) để là null.
            if (string.IsNullOrWhiteSpace(inputImagePath))
            {
                inputImagePath = null;
                return;
            }

            // Nếu tên ảnh là "No-Image.png" thì chuyển thành null.
            if (inputImagePath == "No-Image.png")
            {
                inputImagePath = null;
                return;
            }

            // Nếu tên ảnh không phải là "No-Images.png" => Đã có thiết đặt phần ảnh.
            // ImagePath lúc này là đường dẫn, không phải tên file lưu trong database.
            if (inputImagePath != "No-Image.png" && !File.Exists(string.Format(@"Images\{0}", Path.GetFileName(inputImagePath))))
            {
                // Nếu file ảnh tồn tại.
                if (File.Exists(inputImagePath))
                {
                    string newName = string.Format("IMG {0}{1}", DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-ffff"), Path.GetExtension(inputImagePath));
                    try
                    {
                        File.Copy(inputImagePath, string.Format(@"Images\{0}", newName));
                        inputImagePath = newName;
                    }
                    catch
                    {
                        inputImagePath = "No-Image.png";
                    }
                }
                else
                {
                    inputImagePath = "No-Image.png";
                }
            }
        }

        /// <summary>
        /// Điều chỉnh lại tên file ảnh trong một số trường hợp đặc biệt.
        /// Chỉ trả về tên file.
        /// </summary>
        /// <param name="inputImagePath"></param>
        private void HandleImagePathWhenLoad(ref string inputImagePath)
        {
            if (inputImagePath == null)
            {
                inputImagePath = "No-Image.png";
                return;
            }

            if (!File.Exists(string.Format(@"Images\{0}", Path.GetFileName(inputImagePath))))
            {
                inputImagePath = "No-Image.png";
            }
        }

        public IEnumerable<int> GetAllMonAnIdInKhongThongKe()
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                var listMonAnKhongThongKe = uow.MonAnRepository.GetAllMonAnIdInKhongThongKe();
                uow.Commit();
                return listMonAnKhongThongKe;
            }
        }

        public bool IsMonAnIdInKhongThongKe(int monAnId)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                bool currentKhongThongKe = uow.MonAnRepository.IsMonAnIdInKhongThongKe(monAnId);
                uow.Commit();
                return currentKhongThongKe;
            }
        }

        public bool InsertMonAnIntoKhongThongKe(int monAnId)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                if (!uow.MonAnRepository.InsertMonAnIntoKhongThongKe(monAnId))
                    return false;
                uow.Commit();
                return true;
            }
        }

        public bool DeleteMonAnIdFromKhongThongKe(int monAnId)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                if (!uow.MonAnRepository.DeleteMonAnIdFromKhongThongKe(monAnId))
                    return false;
                uow.Commit();
                return true;
            }
        }

        public IEnumerable<MonAn> GetMonAnByDanhMucIdIncludedDefinedDanhMuc(int? danhMucId)
        {
            if (danhMucId == DanhMucHeThong.ChuaPhanLoai.Id || danhMucId == null)
                return GetMonAnInDanhMucChuaPhanLoai();
            else if (danhMucId == DanhMucHeThong.TatCa.Id)
                return GetAllMonAn();
            else if (danhMucId == DanhMucHeThong.Top10.Id)
                return GetTop10MonAnBanChayTatCaChiNhanhTrongThang();
            else
                return GetMonAnByDanhMucId(danhMucId.Value);
        }
    }
}
