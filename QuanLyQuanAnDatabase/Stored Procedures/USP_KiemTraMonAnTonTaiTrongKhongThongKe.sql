﻿CREATE PROCEDURE [dbo].[USP_KiemTraMonAnTonTaiTrongKhongThongKe]
	@MonAnId						int,
	@Result							int OUTPUT
AS
	IF @MonAnId IS NULL
		RETURN 1;

	IF NOT EXISTS (SELECT * FROM [KhongThongKe] KTK WHERE KTK.MonAnId = @MonAnId)
		SET @Result = 0;
	ELSE
		SET @Result = 1;

	RETURN 0;
