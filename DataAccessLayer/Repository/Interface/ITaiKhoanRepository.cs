﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;

namespace QuanLyQuanAn.DataAccessLayer
{
    public interface ITaiKhoanRepository
    {
        TaiKhoan GetTaiKhoan(int id);
        TaiKhoan GetTaiKhoan(TaiKhoan taiKhoan);
        TaiKhoan GetTaiKhoanByUserName(string userName);

        IEnumerable<TaiKhoan> GetAll();
        bool DeleteTaiKhoan(int id);
        bool DeleteTaiKhoan(TaiKhoan taiKhoan);
        bool UpdateTaiKhoan(TaiKhoan taiKhoan);
    }
}
