﻿CREATE TABLE [dbo].[NhanVien]
(
	Id					int IDENTITY(1,1),
	HoTen				nvarchar(255),
	SoCmnd				nvarchar(255),
	SoDienThoai			nvarchar(255),
	DiaChi				nvarchar(255),
	ThoiGianTao			datetime DEFAULT GETDATE(),
	ThoiGianCapNhat		datetime DEFAULT GETDATE(),

	CONSTRAINT PK_NhanVien PRIMARY KEY (Id),
	CONSTRAINT AK_NhanVien UNIQUE (SoCmnd)
);
