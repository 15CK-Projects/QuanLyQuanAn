﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanAn.DataObject
{
    public class BanChiNhanh
    {
        private int? _id;
        private string _tenBan;
        private int? _chiNhanhId;
        private string _moTa;
        private DateTime? _thoiGianTao;
        private DateTime? _thoiGianCapNhat;

        public int? Id { get => _id; set => _id = value; }
        public string TenBan { get => _tenBan; set => _tenBan = value; }
        public int? ChiNhanhId { get => _chiNhanhId; set => _chiNhanhId = value; }
        public string MoTa { get => _moTa; set => _moTa = value; }
        public DateTime? ThoiGianTao { get => _thoiGianTao; set => _thoiGianTao = value; }
        public DateTime? ThoiGianCapNhat { get => _thoiGianCapNhat; set => _thoiGianCapNhat = value; }
    }
}
