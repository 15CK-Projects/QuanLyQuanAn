﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanAn.DataObject
{
    public class ChiNhanh
    {
        private int? _id;
        private string _tenChiNhanh;
        private string _soDienThoai;
        private string _diaChi;
        private DateTime? _thoiGianTao;
        private DateTime? _thoiGianCapNhat;

        public int? Id { get => _id; set => _id = value; }
        public string TenChiNhanh { get => _tenChiNhanh; set => _tenChiNhanh = value; }
        public string SoDienThoai { get => _soDienThoai; set => _soDienThoai = value; }
        public string DiaChi { get => _diaChi; set => _diaChi = value; }
        public DateTime? ThoiGianTao { get => _thoiGianTao; set => _thoiGianTao = value; }
        public DateTime? ThoiGianCapNhat { get => _thoiGianCapNhat; set => _thoiGianCapNhat = value; }
    }
}
