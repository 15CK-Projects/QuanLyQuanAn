﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.ComponentModel;
using QuanLyQuanAn.Service;
using QuanLyQuanAn.DataObject;
using QuanLyQuanAn.Common;

namespace QuanLyQuanAn.GUI.BoPhanQuanLyGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /*
         * QUY ƯỚC ĐẶT TÊN: 
         * Biến toàn cục: _PhânHệ_TênBiến
         * Biến cục bộ: tênBiến
         * 
         * Phân hệ:
         * _Main
         * _DanhMuc
         * _MonAn
         * _ChiNhanh
         * _ChiNhanh_MonAnChiNhanh (a.k.a Menu)
         * _ChiNhanh_BanChiNhanh
        */

        private DialogManager _Main_DialogManager = null;

        private IDanhMucService _Main_DanhMucService = new DanhMucService();
        private IMonAnService _Main_MonAnService = new MonAnService();
        private IChiNhanhService _Main_ChiNhanhService = new ChiNhanhService();

        private INhanVienService _Main_NhanVienService = new NhanVienService();
        private ITaiKhoanService _Main_TaiKhoanService = new TaiKhoanService();

        private IThongKeService _Main_ThongKeService = new ThongKeService();

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            System.Globalization.CultureInfo.CurrentCulture = System.Globalization.CultureInfo.GetCultureInfo("vi-VN");

            // Phải new trong _Loaded event để set Owner Window được.
            _Main_DialogManager = new DialogManager(this);

            Button_Profile.IsEnabled = false;
            Button_Logout.IsEnabled = false;

            DisableMenu();
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string title = null;
            if (TabItem_DanhMuc.IsSelected)
                title = "Danh mục";
            else if (TabItem_MonAn.IsSelected)
                title = "Món ăn";
            else if (TabItem_ChiNhanh.IsSelected)
                title = "Chi nhánh";
            else if (TabItem_MonAn.IsSelected)
                title = "Menu";
            else if (TabItem_BanChiNhanh.IsSelected)
                title = "Bàn";
            else if (TabItem_BaoCaoThongKe.IsSelected)
                title = "Thống kê và Báo cáo";
            else if (TabItem_PhanQuyen.IsSelected)
                title = "Phân quyền";
            else if (TabItem_TaiKhoan.IsSelected)
                title = "Tài khoản";
            else if (TabItem_Login.IsSelected)
                title = "Đăng nhập";
            else if (TabItem_Profile.IsSelected)
                title = "Thông tin người dùng";
            else
                title = "";
            TextBlock_Header.Text = title;
        }
    }
}
