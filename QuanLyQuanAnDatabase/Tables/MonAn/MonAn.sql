﻿CREATE TABLE [dbo].[MonAn]
(
	Id					int IDENTITY(1,1),
	TenMonAn			nvarchar(255) NOT NULL,
	DanhMucId			int,
	MoTa				ntext,
	HinhAnh				ntext,
	GiaThamKhao			decimal(15,4), -- Giá tham khảo.
	ThoiGianTao			datetime,
	ThoiGianCapNhat		datetime,

	CONSTRAINT PK_MonAn PRIMARY KEY (Id)
);
