﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;
using Dapper;

namespace QuanLyQuanAn.DataAccessLayer
{
    internal class DanhMucRepository : RepositoryBase, IDanhMucRepository
    {
        public DanhMucRepository(IDbTransaction transaction) : base(transaction)
        {

        }

        public bool DeleteDanhMuc(int id)
        {
            string sql = @"DELETE FROM [DanhMuc] WHERE Id = @Id;";
            int rowsCount = Connection.Execute(sql: sql, param: new { Id = id }, transaction: Transaction);
            if (rowsCount == 0)
                return false;
            return true;
        }

        public bool DeleteDanhMuc(DanhMuc danhMuc)
        {
            if (danhMuc == null || !danhMuc.Id.HasValue)
                throw new ArgumentNullException("danhMuc");
            return DeleteDanhMuc(danhMuc.Id.Value);
        }

        public IEnumerable<DanhMuc> GetAll()
        {
            string sql = @"SELECT * FROM [DanhMuc] ORDER BY TenDanhMuc ASC;";
            var queryResult = Connection.Query<DanhMuc>(sql: sql, param: null, transaction: Transaction);
            return queryResult;
        }

        public DanhMuc GetDanhMuc(int id)
        {
            string sql = @"SELECT * FROM [DanhMuc] WHERE Id = @Id";
            return Connection.QuerySingleOrDefault<DanhMuc>(sql: sql, param: new { Id = id }, transaction: Transaction);
        }

        public DanhMuc GetDanhMuc(DanhMuc danhMuc)
        {
            if (danhMuc == null || !danhMuc.Id.HasValue)
            {
                throw new ArgumentNullException("danhMuc");
            }
            return GetDanhMuc(danhMuc.Id.Value);
        }

        public bool InsertDanhMuc(DanhMuc danhMuc)
        {
            string sql = @"INSERT INTO [DanhMuc] (TenDanhMuc, MoTa, HinhAnh) VALUES (@TenDanhMuc, @MoTa, @HinhAnh);
                    SELECT CAST(SCOPE_IDENTITY() AS int);";
            try
            {
                danhMuc.Id = Connection.ExecuteScalar<int>(sql: sql, param: danhMuc, transaction: Transaction);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateDanhMuc(DanhMuc danhMuc)
        {
            string sql = @"UPDATE [DanhMuc] SET TenDanhMuc = @TenDanhMuc, MoTa = @MoTa, HinhAnh = @HinhAnh, ThoiGianCapNhat = @ThoiGianCapNhat WHERE Id = @Id;";
            danhMuc.ThoiGianCapNhat = DateTime.Now;
            int rowsCount = Connection.Execute(sql: sql, param: danhMuc, transaction: Transaction);
            if (rowsCount == 0)
                return false;
            return true;
        }
    }
}
