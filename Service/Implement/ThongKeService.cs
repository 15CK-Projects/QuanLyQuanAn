﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;
using QuanLyQuanAn.DataAccessLayer;

namespace QuanLyQuanAn.Service
{
    public class ThongKeService : IThongKeService
    {
        public IEnumerable<MonAn> GetTop10MonAnBanChayTatCaChiNhanhTrongThang()
        {
            DateTime from = new DateTime(year: DateTime.Now.Year, month: DateTime.Now.Month, day: 1, hour: 0, minute: 0, second: 0, millisecond: 0);
            DateTime to = new DateTime(year: DateTime.Now.Year,
                month: DateTime.Now.Month,
                day: DateTime.DaysInMonth(year: DateTime.Now.Year, month: DateTime.Now.Month),
                hour: 23, minute: 59, second: 59, millisecond: 999);

            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                var result = uow.ThongKeRepository.GetTop10MonAnBanChayTatCaChiNhanh(from, to);
                uow.Commit();
                return result;
            }
        }
    }
}
