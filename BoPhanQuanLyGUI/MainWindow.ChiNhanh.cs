﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.ComponentModel;
using QuanLyQuanAn.Service;
using QuanLyQuanAn.DataObject;
using QuanLyQuanAn.Common;

namespace QuanLyQuanAn.GUI.BoPhanQuanLyGUI
{
    public partial class MainWindow : Window
    {
        private class MonAnChiNhanhComposite
        {
            public MonAnChiNhanh MonAnChiNhanh { get; set; }
            public MonAn MonAn { get; set; }
        }

        private readonly ObservableCollection<ChiNhanh> _ChiNhanh_ListChiNhanh = new ObservableCollection<ChiNhanh>();

        private readonly ObservableCollection<MonAnChiNhanhComposite> _ChiNhanh_MonAnChiNhanh_ListMonAnChiNhanh = new ObservableCollection<MonAnChiNhanhComposite>();
        private readonly ObservableCollection<MonAn> _ChiNhanh_MonAnChiNhanh_ListMonAn = new ObservableCollection<MonAn>();

        private readonly ObservableCollection<BanChiNhanh> _ChiNhanh_BanChiNhanh_ListBanChiNhanh = new ObservableCollection<BanChiNhanh>();

        private CollectionViewSource _ChiNhanh_ListChiNhanh_Source = null;
        private ICollectionView _ChiNhanh_ListChiNhanh_View = null;

        private CollectionViewSource _ChiNhanh_MonAnChiNhanh_ListMonAn_Source = null;
        private ICollectionView _ChiNhanh_MonAnChiNhanh_ListMonAn_View = null;

        #region Chi nhánh
        private void Button_ChiNhanh_Click(object sender, RoutedEventArgs e)
        {
			// Setup View.
            _ChiNhanh_ListChiNhanh_Source = new CollectionViewSource() { Source = _ChiNhanh_ListChiNhanh };
            _ChiNhanh_ListChiNhanh_View = _ChiNhanh_ListChiNhanh_Source.View;

			// Load ChiNhanh from database.
            var listChiNhanh = _Main_ChiNhanhService.GetAllChiNhanh();
            _ChiNhanh_ListChiNhanh.Clear();
            foreach (ChiNhanh chiNhanh in listChiNhanh)
                _ChiNhanh_ListChiNhanh.Add(chiNhanh);

            DataGrid_ChiNhanh.ItemsSource = _ChiNhanh_ListChiNhanh_View;

            TabItem_ChiNhanh.IsSelected = true;
        }

        private void DataGrid_ChiNhanh_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ChiNhanh selectedChiNhanh = DataGrid_ChiNhanh.SelectedItem as ChiNhanh;
            if (selectedChiNhanh == null)
            {
                Button_DeleteChiNhanh.IsEnabled = false;
                Button_UpdateChiNhanh.IsEnabled = false;
                Button_Menu.IsEnabled = false;
                Button_Table.IsEnabled = false;
                return;
            }
            else
            {
                Button_DeleteChiNhanh.IsEnabled = true;
                Button_UpdateChiNhanh.IsEnabled = true;
                Button_Menu.IsEnabled = true;
                Button_Table.IsEnabled = true;
            }

            TextBox_TenChiNhanh.Text = selectedChiNhanh.TenChiNhanh;
            TextBox_SoDienThoai.Text = selectedChiNhanh.SoDienThoai;
            TextBox_DiaChi.Text = selectedChiNhanh.DiaChi;
        }

        private void Button_AddChiNhanh_Click(object sender, RoutedEventArgs e)
        {
            ChiNhanh newChiNhanh = new ChiNhanh();
            newChiNhanh.TenChiNhanh = TextBox_TenChiNhanh.Text;
            newChiNhanh.SoDienThoai = TextBox_SoDienThoai.Text;
            newChiNhanh.DiaChi = TextBox_DiaChi.Text;

            if (_Main_ChiNhanhService.AddChiNhanh(newChiNhanh))
            {
                _ChiNhanh_ListChiNhanh.Add(newChiNhanh);
                _Main_DialogManager.CreateMessageDialog()
                   .SetCaption("Thông báo")
                   .SetMessage("Đã thêm chi nhánh mới thành công!")
                   .SetButton(DialogManager.DialogButton.OK)
                   .SetImage(DialogManager.DialogImage.Information).ShowDialog();
            }
            else
            {
                _Main_DialogManager.CreateMessageDialog()
                   .SetCaption("Thông báo")
                   .SetMessage("Không thêm được chi nhánh mới!")
                   .SetButton(DialogManager.DialogButton.OK)
                   .SetImage(DialogManager.DialogImage.Error).ShowDialog();
            }
        }

        private void Button_DeleteChiNhanh_Click(object sender, RoutedEventArgs e)
        {
            ChiNhanh selectedChiNhanh = DataGrid_ChiNhanh.SelectedItem as ChiNhanh;
            if (selectedChiNhanh == null)
                return;

            var comfirmDialog = _Main_DialogManager.CreateMessageDialog()
                   .SetCaption("Thông báo")
                   .SetMessage("Bạn có chắc chắn rằng muốn xóa chi nhánh này không?")
                   .SetButton(DialogManager.DialogButton.YesNo)
                   .SetImage(DialogManager.DialogImage.Question);
            comfirmDialog.ShowDialog();
            var comfirmResutl = comfirmDialog.MessageDialogResult;

            if (comfirmResutl == DialogManager.DialogResult.No)
                return;

            if (_Main_ChiNhanhService.DeleteChiNhanh(selectedChiNhanh.Id.Value))
            {
                _ChiNhanh_ListChiNhanh.Remove(selectedChiNhanh);
            }
            else
            {
                _Main_DialogManager.CreateMessageDialog()
                   .SetCaption("Thông báo")
                   .SetMessage("Không xóa được chi nhánh!")
                   .SetButton(DialogManager.DialogButton.OK)
                   .SetImage(DialogManager.DialogImage.Error).ShowDialog();
            }
        }

        private void Button_UpdateChiNhanh_Click(object sender, RoutedEventArgs e)
        {
            ChiNhanh selectedChiNhanh = DataGrid_ChiNhanh.SelectedItem as ChiNhanh;
            if (selectedChiNhanh == null)
                return;

            // Backup old values.
            string tenChiNhanh = selectedChiNhanh.TenChiNhanh;
            string diaChi = selectedChiNhanh.DiaChi;
            string soDienThoai = selectedChiNhanh.SoDienThoai;

            // Assign new values.
            selectedChiNhanh.TenChiNhanh = TextBox_TenChiNhanh.Text;
            selectedChiNhanh.SoDienThoai = TextBox_SoDienThoai.Text;
            selectedChiNhanh.DiaChi = TextBox_DiaChi.Text;

            if (_Main_ChiNhanhService.UpdateChiNhanh(selectedChiNhanh))
            {
                DataGrid_ChiNhanh.Items.Refresh();
                _Main_DialogManager.CreateMessageDialog()
                      .SetCaption("Thông báo")
                      .SetMessage("Đã cập nhật chi nhánh thành công!")
                      .SetButton(DialogManager.DialogButton.OK)
                      .SetImage(DialogManager.DialogImage.Information).ShowDialog();
            }
            else
            {
                // Restore old values.
                selectedChiNhanh.TenChiNhanh = tenChiNhanh;
                selectedChiNhanh.SoDienThoai = soDienThoai;
                selectedChiNhanh.DiaChi = diaChi;

                _Main_DialogManager.CreateMessageDialog()
                      .SetCaption("Thông báo")
                      .SetMessage("Không cập nhật được chi nhánh!")
                      .SetButton(DialogManager.DialogButton.OK)
                      .SetImage(DialogManager.DialogImage.Error).ShowDialog();
            }
        }

        private void Button_ChiNhanh_Filter_Click(object sender, RoutedEventArgs e)
        {
            DataGrid_ChiNhanh.Background = Brushes.LightGoldenrodYellow;
            _ChiNhanh_ListChiNhanh_View.Filter = new Predicate<object>(ChiNhanhFilter);
        }

        private void Button_ChiNhanh_CancelFilter_Click(object sender, RoutedEventArgs e)
        {
            DataGrid_ChiNhanh.ClearValue(BackgroundProperty);
            _ChiNhanh_ListChiNhanh_View.Filter = null;
        }

        private bool ChiNhanhFilter(object obj)
        {
            ChiNhanh chiNhanh = obj as ChiNhanh;
            string tenChiNhanhFilter = TextBox_ChiNhanh_Filter_TenChiNhanh.Text;
            string diaChiFilter = TextBox_ChiNhanh_Filter_DiaChi.Text;
            bool isOK = true;
            var compareInfo = System.Globalization.CultureInfo.InvariantCulture.CompareInfo;

            if ((chiNhanh.TenChiNhanh != null) && (CheckBox_ChiNhanh_Filter_TenChiNhanh.IsChecked == true) && !string.IsNullOrWhiteSpace(tenChiNhanhFilter))
            {
                if (compareInfo.IndexOf(source: chiNhanh.TenChiNhanh, value: tenChiNhanhFilter, options: System.Globalization.CompareOptions.IgnoreCase | System.Globalization.CompareOptions.IgnoreNonSpace) == -1)
                    isOK = false;
            }

            if ((chiNhanh.DiaChi != null) && (CheckBox_ChiNhanh_Filter_DiaChi.IsChecked == true) && !string.IsNullOrWhiteSpace(diaChiFilter))
            {
                if (compareInfo.IndexOf(source: chiNhanh.DiaChi, value: diaChiFilter, options: System.Globalization.CompareOptions.IgnoreCase | System.Globalization.CompareOptions.IgnoreNonSpace) == -1)
                    isOK = false;
            }
            return isOK;
        }
        #endregion

        #region MonAnChiNhanh
        private void Button_Menu_Click(object sender, RoutedEventArgs e)
        {
            var selectedChiNhanh = DataGrid_ChiNhanh.SelectedItem as ChiNhanh;
            if (selectedChiNhanh == null)
                return;

            // Lấy danh sách món ăn của chi nhánh.
            var listMonAnChiNhanh = _Main_ChiNhanhService.FindMonAnChiNhanhByChiNhanhId(selectedChiNhanh.Id.Value);
            _ChiNhanh_MonAnChiNhanh_ListMonAnChiNhanh.Clear();
            foreach (MonAnChiNhanh monAnChiNhanh in listMonAnChiNhanh)
            {
                MonAnChiNhanhComposite entry = new MonAnChiNhanhComposite();
                entry.MonAnChiNhanh = monAnChiNhanh;
                entry.MonAn = _Main_MonAnService.GetMonAn(monAnChiNhanh.MonAnId.Value);
                _ChiNhanh_MonAnChiNhanh_ListMonAnChiNhanh.Add(entry);
            }
            DataGrid_Menu.ItemsSource = _ChiNhanh_MonAnChiNhanh_ListMonAnChiNhanh;

            _ChiNhanh_MonAnChiNhanh_ListMonAn_Source = new CollectionViewSource() { Source = _ChiNhanh_MonAnChiNhanh_ListMonAn };
            _ChiNhanh_MonAnChiNhanh_ListMonAn_View = _ChiNhanh_MonAnChiNhanh_ListMonAn_Source.View;

            TabItem_Menu.IsSelected = true;
            TabItem_Menu_Edit.IsSelected = true;
        }

        private void Button_AddMonAnChiNhanh_Click(object sender, RoutedEventArgs e)
        {
            var listDanhMuc = _Main_DanhMucService.GetAllDanhMuc();
            ComboBox_Menu_Filter_DanhMuc.ItemsSource = listDanhMuc;
            ComboBox_Menu_Filter_DanhMuc.SelectedItem = listDanhMuc.SingleOrDefault(o => o.Id == DanhMucHeThong.TatCa.Id);

            _ChiNhanh_MonAnChiNhanh_ListMonAn_View.Filter = null;
            DataGrid_Menu_MonAn.ItemsSource = _ChiNhanh_MonAnChiNhanh_ListMonAn_View;
            TabItem_Menu_AddMonAn.IsSelected = true;
        }

        private void ComboBox_Menu_Filter_DanhMuc_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _ChiNhanh_MonAnChiNhanh_ListMonAn.Clear();

            DanhMuc selectedDanhMuc = ComboBox_Menu_Filter_DanhMuc.SelectedItem as DanhMuc;
            if (selectedDanhMuc == null)
            {
                return;
            }

            IEnumerable<MonAn> listMonAn = _Main_MonAnService.GetMonAnByDanhMucIdIncludedDefinedDanhMuc(selectedDanhMuc.Id);

            foreach (MonAn monAn in listMonAn)
            {
                _ChiNhanh_MonAnChiNhanh_ListMonAn.Add(monAn);
            }
        }

        private bool MonAnFilterInMenu(object obj)
        {
            MonAn monAn = obj as MonAn;
            string tenMonAnFilter = TextBox_Menu_Filter_TenMonAn.Text;
            var compareInfo = System.Globalization.CultureInfo.InvariantCulture.CompareInfo;
            bool isOk = true;
            if (CheckBox_Menu_Filter_TenMonAn.IsChecked == true && !string.IsNullOrWhiteSpace(tenMonAnFilter))
            {
                if (compareInfo.IndexOf(source: monAn.TenMonAn, value: tenMonAnFilter, options: System.Globalization.CompareOptions.IgnoreCase | System.Globalization.CompareOptions.IgnoreNonSpace) == -1)
                    isOk = false;
            }
            return isOk;
        }

        private void Button_Menu_Filter_Click(object sender, RoutedEventArgs e)
        {
            _ChiNhanh_MonAnChiNhanh_ListMonAn_View.Filter = new Predicate<object>(MonAnFilterInMenu);
        }

        private void Button_Menu_CancelFilter_Click(object sender, RoutedEventArgs e)
        {
            _ChiNhanh_MonAnChiNhanh_ListMonAn_View.Filter = null;
        }

        private void DataGrid_Menu_MonAn_MouseMove(object sender, MouseEventArgs e)
        {
            DataGrid dataGridMonAn = sender as DataGrid;
            List<MonAn> listMonAn = new List<MonAn>();
            foreach (var selectedItem in dataGridMonAn.SelectedItems)
            {
                if (selectedItem is MonAn)
                    listMonAn.Add(selectedItem as MonAn);
            }
            if (dataGridMonAn != null && e.LeftButton == MouseButtonState.Pressed)
            {
                DragDrop.DoDragDrop(dataGridMonAn, listMonAn, DragDropEffects.Copy);
            }
        }

        private void DataGrid_Menu_Drop(object sender, DragEventArgs e)
        {
            DataGrid dataGridMonAnChiNhanh = sender as DataGrid;
            if (dataGridMonAnChiNhanh == null)
                return;
            var danhSachMonAn = e.Data.GetData(typeof(List<MonAn>)) as List<MonAn>;
            if (danhSachMonAn == null)
                return;
            ChiNhanh selectedChiNhanh = DataGrid_ChiNhanh.SelectedItem as ChiNhanh;
            foreach (MonAn monAn in danhSachMonAn)
            {
                if (_ChiNhanh_MonAnChiNhanh_ListMonAnChiNhanh.FirstOrDefault(o => o.MonAnChiNhanh.MonAnId == monAn.Id) == null)
                {
                    MonAnChiNhanhComposite newMonAnChiNhanh = new MonAnChiNhanhComposite();
                    newMonAnChiNhanh.MonAn = _Main_MonAnService.GetMonAn(monAn.Id.Value);
                    newMonAnChiNhanh.MonAnChiNhanh = new MonAnChiNhanh() { ChiNhanhId = selectedChiNhanh.Id, MonAnId = newMonAnChiNhanh.MonAn.Id, GiaBan = newMonAnChiNhanh.MonAn.GiaThamKhao };
                    _Main_ChiNhanhService.AddMonAnChiNhanh(newMonAnChiNhanh.MonAnChiNhanh);
                    _ChiNhanh_MonAnChiNhanh_ListMonAnChiNhanh.Add(newMonAnChiNhanh);
                }
            }
        }

        private void DataGrid_Menu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedMonAnChiNhanhComposite = DataGrid_Menu.SelectedItem as MonAnChiNhanhComposite;
            if (selectedMonAnChiNhanhComposite == null)
                return;

            TextBox_Menu_TenMonAn.Text = selectedMonAnChiNhanhComposite.MonAn.TenMonAn;
            TextBox_Menu_GiaMonAn.Text = string.Format("{0:N0}", selectedMonAnChiNhanhComposite.MonAnChiNhanh.GiaBan ?? 0M);
        }

        private void Button_Menu_BackToEdit_Click(object sender, RoutedEventArgs e)
        {
            TabItem_Menu_Edit.IsSelected = true;
        }

        private void Button_DeleteMonAnChiNhanh_Click(object sender, RoutedEventArgs e)
        {
            var selectedMonAnChiNhanhComposite = DataGrid_Menu.SelectedItem as MonAnChiNhanhComposite;
            if (selectedMonAnChiNhanhComposite == null)
                return;

            if (!_Main_ChiNhanhService.DeleteMonAnChiNhanh(selectedMonAnChiNhanhComposite.MonAnChiNhanh))
            {
                _Main_DialogManager.CreateMessageDialog().SetCaption("Thông báo").SetMessage("Không xóa được!").SetImage(DialogManager.DialogImage.Error).ShowDialog();
                return;
            }

            _ChiNhanh_MonAnChiNhanh_ListMonAnChiNhanh.Remove(selectedMonAnChiNhanhComposite);
        }

        private void Button_UpdateMonAnChiNhanh_Click(object sender, RoutedEventArgs e)
        {
            var selectedMonAnChiNhanhComposite = DataGrid_Menu.SelectedItem as MonAnChiNhanhComposite;
            if (selectedMonAnChiNhanhComposite == null)
                return;

            // Backup old values.
            decimal? giaBan = selectedMonAnChiNhanhComposite.MonAnChiNhanh.GiaBan;

            // Set new values.
            decimal newGiaBan;
            if (!decimal.TryParse(TextBox_Menu_GiaMonAn.Text, System.Globalization.NumberStyles.AllowThousands, System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), out newGiaBan))
            {
                newGiaBan = 0M;
            }
            selectedMonAnChiNhanhComposite.MonAnChiNhanh.GiaBan = newGiaBan;

            if (!_Main_ChiNhanhService.UpdateMonAnChiNhanh(selectedMonAnChiNhanhComposite.MonAnChiNhanh))
            {
                // Restore old values.
                selectedMonAnChiNhanhComposite.MonAnChiNhanh.GiaBan = giaBan;
                _Main_DialogManager.CreateMessageDialog().SetCaption("Thông báo").SetMessage("Không cập nhật được!").SetImage(DialogManager.DialogImage.Error).ShowDialog();
                return;
            }

            DataGrid_Menu.Items.Refresh();
        }

        private void Button_Duplicate_Click(object sender, RoutedEventArgs e)
        {
            ComboBox_Menu_Duplicate_ChiNhanh.ItemsSource = _ChiNhanh_ListChiNhanh;
            TabItem_Menu_Duplicate.IsSelected = true;
        }

        private void Button_DeleteAllMonAnChiNhanh_Click(object sender, RoutedEventArgs e)
        {
            ChiNhanh chiNhanhTo = DataGrid_ChiNhanh.SelectedItem as ChiNhanh;
            if (chiNhanhTo == null)
                return;
            ChiNhanh chiNhanhFrom = ComboBox_Menu_Duplicate_ChiNhanh.SelectedItem as ChiNhanh;
            if (chiNhanhFrom == null)
                return;

            var confirmDialog = _Main_DialogManager.CreateMessageDialog().SetCaption("Xác nhận").SetMessage("Việc này sẽ khiến cho toàn bộ thông tin về bàn của chi nhánh hiện tại bị xóa! Bạn có muốn tiếp tục?").SetButton(DialogManager.DialogButton.YesNo).SetImage(DialogManager.DialogImage.Warning);
            confirmDialog.ShowDialog();
            if (confirmDialog.MessageDialogResult == DialogManager.DialogResult.No)
            {
                return;
            }

            if (!_Main_ChiNhanhService.DuplicateMonAnChiNhanh(chiNhanhFrom.Id.Value, chiNhanhTo.Id.Value))
            {
                _Main_DialogManager.CreateMessageDialog().SetCaption("Thông báo").SetMessage("Không thực hiện được!").SetImage(DialogManager.DialogImage.Error).ShowDialog();
                return;
            }

            var listMonAnChiNhanh = _Main_ChiNhanhService.FindMonAnChiNhanhByChiNhanhId(chiNhanhTo.Id.Value);
            _ChiNhanh_MonAnChiNhanh_ListMonAnChiNhanh.Clear();
            foreach (MonAnChiNhanh monAnChiNhanh in listMonAnChiNhanh)
            {
                MonAnChiNhanhComposite entry = new MonAnChiNhanhComposite();
                entry.MonAnChiNhanh = monAnChiNhanh;
                entry.MonAn = _Main_MonAnService.GetMonAn(monAnChiNhanh.MonAnId.Value);
                _ChiNhanh_MonAnChiNhanh_ListMonAnChiNhanh.Add(entry);
            }
            DataGrid_Menu.ItemsSource = _ChiNhanh_MonAnChiNhanh_ListMonAnChiNhanh;
        }

        private void Button_Menu_Duplicate_Action_Click(object sender, RoutedEventArgs e)
        {
            ChiNhanh currentChiNhanh = DataGrid_ChiNhanh.SelectedItem as ChiNhanh;
            ChiNhanh selectedChiNhanh = ComboBox_Menu_Duplicate_ChiNhanh.SelectedItem as ChiNhanh;
            if (selectedChiNhanh == null)
            {
                _Main_DialogManager.CreateMessageDialog()
                    .SetCaption("Cảnh báo")
                    .SetMessage("Bắt buộc lựa chọn một chi nhánh để tiến hành.")
                    .SetImage(DialogManager.DialogImage.Warning)
                    .ShowDialog();
                return;
            }
            var listMonAnChiNhanh = _Main_ChiNhanhService.FindMonAnChiNhanhByChiNhanhId(selectedChiNhanh.Id.Value);
            foreach (MonAnChiNhanh item in listMonAnChiNhanh)
            {
                item.ChiNhanhId = currentChiNhanh.Id;
                _Main_ChiNhanhService.AddMonAnChiNhanh(item);
            }

            var reload = _Main_ChiNhanhService.FindMonAnChiNhanhByChiNhanhId(selectedChiNhanh.Id.Value);
            _ChiNhanh_MonAnChiNhanh_ListMonAnChiNhanh.Clear();
            foreach (MonAnChiNhanh item in reload)
            {
                MonAnChiNhanhComposite entry = new MonAnChiNhanhComposite();
                entry.MonAnChiNhanh = item;
                entry.MonAn = _Main_MonAnService.GetMonAn(item.MonAnId.Value);
                _ChiNhanh_MonAnChiNhanh_ListMonAnChiNhanh.Add(entry);
            }
            TabItem_Menu_Edit.IsSelected = true;
        }

        private void Button_Menu_Duplicate_Back_Click(object sender, RoutedEventArgs e)
        {
            TabItem_Menu_Edit.IsSelected = true;
        }

        private void Button_Menu_Back_Click(object sender, RoutedEventArgs e)
        {
            TabItem_ChiNhanh.IsSelected = true;
        }
        #endregion

        #region BanChiNhanh
        private void Button_Ban_Click(object sender, RoutedEventArgs e)
        {
            var selectedChiNhanh = DataGrid_ChiNhanh.SelectedItem as ChiNhanh;
            if (selectedChiNhanh == null)
                return;

            var listBanChiNhanh = _Main_ChiNhanhService.FindBanByChiNhanhId(selectedChiNhanh.Id.Value);
            _ChiNhanh_BanChiNhanh_ListBanChiNhanh.Clear();
            foreach (BanChiNhanh ban in listBanChiNhanh)
            {
                _ChiNhanh_BanChiNhanh_ListBanChiNhanh.Add(ban);
            }
            DataGrid_BanChiNhanh.ItemsSource = _ChiNhanh_BanChiNhanh_ListBanChiNhanh;
            TabItem_BanChiNhanh.IsSelected = true;
        }

        private void DataGrid_BanChiNhanh_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BanChiNhanh selectedBanChiNhanh = DataGrid_BanChiNhanh.SelectedItem as BanChiNhanh;
            if (selectedBanChiNhanh == null)
                return;

            TextBox_BanChiNhanh_TenBan.Text = selectedBanChiNhanh.TenBan;
            TextBox_BanChiNhanh_MoTa.Text = selectedBanChiNhanh.MoTa;
        }

        private void Button_AddBanChiNhanh_Click(object sender, RoutedEventArgs e)
        {
            ChiNhanh currentChiNhanh = DataGrid_ChiNhanh.SelectedItem as ChiNhanh;
            if (currentChiNhanh == null)
                return;

            var newBan = new BanChiNhanh()
            {
                ChiNhanhId = currentChiNhanh.Id,
                TenBan = TextBox_BanChiNhanh_TenBan.Text,
                MoTa = TextBox_BanChiNhanh_MoTa.Text
            };
            

			if (!_Main_ChiNhanhService.AddBanChiNhanh(newBan))
            {
                _Main_DialogManager.CreateMessageDialog().SetCaption("Thông báo").SetMessage("Không thêm được bàn!").SetImage(DialogManager.DialogImage.Error).ShowDialog();
                return;
            }

            _ChiNhanh_BanChiNhanh_ListBanChiNhanh.Add(newBan);
        }

        private void Button_DeleteBanChiNhanh_Click(object sender, RoutedEventArgs e)
        {
            BanChiNhanh selectedBanChiNhanh = DataGrid_BanChiNhanh.SelectedItem as BanChiNhanh;
            if (selectedBanChiNhanh == null)
                return;

            if (!_Main_ChiNhanhService.DeleteBanChiNhanh(selectedBanChiNhanh.Id.Value))
            {
                _Main_DialogManager.CreateMessageDialog().SetCaption("Thông báo").SetMessage("Không xóa được bàn!").SetImage(DialogManager.DialogImage.Error).ShowDialog();
                return;

            }
            _ChiNhanh_BanChiNhanh_ListBanChiNhanh.Remove(selectedBanChiNhanh);
        }

        private void Button_UpdateBanChiNhanh_Click(object sender, RoutedEventArgs e)
        {
            BanChiNhanh selectedBan = DataGrid_BanChiNhanh.SelectedItem as BanChiNhanh;
            if (selectedBan == null)
                return;

            // Backup old values.
            string tenBan = selectedBan.TenBan;

            // Set new values.
            selectedBan.TenBan = TextBox_BanChiNhanh_TenBan.Text;

			if (!_Main_ChiNhanhService.UpdateBanChiNhanh(selectedBan))
            {
                // Restore old values.
                selectedBan.TenBan = tenBan;
                _Main_DialogManager.CreateMessageDialog().SetCaption("Thông báo").SetMessage("Không thêm cập nhật được thông tin bàn!").SetImage(DialogManager.DialogImage.Error).ShowDialog();
                return;
            }

            DataGrid_BanChiNhanh.Items.Refresh();
        }

        private void Button_DeleteAllBanChiNhanh_Click(object sender, RoutedEventArgs e)
        {
            ChiNhanh selectedChiNhanh = DataGrid_ChiNhanh.SelectedItem as ChiNhanh;
            if (selectedChiNhanh == null)
                return;

            var confirmDialog = _Main_DialogManager.CreateMessageDialog().SetCaption("Xác nhận").SetMessage("Bạn có muốn xóa hết thông tin bàn của chi nhánh này không?").SetButton(DialogManager.DialogButton.YesNo).SetImage(DialogManager.DialogImage.Question);
            confirmDialog.ShowDialog();
			if (confirmDialog.MessageDialogResult == DialogManager.DialogResult.Yes)
            {
				if (!_Main_ChiNhanhService.DeleteAllBanChiNhanh(selectedChiNhanh.Id.Value))
                {
                    _Main_DialogManager.CreateMessageDialog().SetCaption("Thông báo").SetMessage("Không thực hiện được!").SetImage(DialogManager.DialogImage.Error).ShowDialog();
                    return;
                }
                _ChiNhanh_BanChiNhanh_ListBanChiNhanh.Clear();
            }
        }

        private void Button_DuplicateBan_Click(object sender, RoutedEventArgs e)
        {
            ComboBox_BanChiNhanh_Duplicate_ChiNhanh.ItemsSource = _ChiNhanh_ListChiNhanh;
            TabItem_BanChiNhanh_Duplicate.IsSelected = true;
        }

        private void Button_BanChiNhanh_Duplicate_Action_Click(object sender, RoutedEventArgs e)
        {
            ChiNhanh toChiNhanh = DataGrid_ChiNhanh.SelectedItem as ChiNhanh;
            if (toChiNhanh == null)
                return;
            ChiNhanh fromChiNhanh = ComboBox_BanChiNhanh_Duplicate_ChiNhanh.SelectedItem as ChiNhanh;
            if (fromChiNhanh == null)
            {
                _Main_DialogManager.CreateMessageDialog()
                    .SetCaption("Cảnh báo")
                    .SetMessage("Bắt buộc lựa chọn một chi nhánh để tiến hành.")
                    .SetImage(DialogManager.DialogImage.Warning)
                    .ShowDialog();
                return;
            }                

            var confirmDialog = _Main_DialogManager.CreateMessageDialog().SetCaption("Xác nhận").SetMessage("Việc này sẽ khiến cho toàn bộ thông tin về bàn của chi nhánh hiện tại bị xóa! Bạn có muốn tiếp tục?").SetButton(DialogManager.DialogButton.YesNo).SetImage(DialogManager.DialogImage.Warning);
            confirmDialog.ShowDialog();
            if (confirmDialog.MessageDialogResult == DialogManager.DialogResult.No)
            {
                return;
            }

            if (!_Main_ChiNhanhService.DuplicateBanChiNhanh(fromChiNhanh.Id.Value, toChiNhanh.Id.Value))
            {
                _Main_DialogManager.CreateMessageDialog().SetCaption("Thông báo").SetMessage("Không thực hiện được!").SetImage(DialogManager.DialogImage.Error).ShowDialog();
                return;
            }

            // Reload.
            var listBanChiNhanh = _Main_ChiNhanhService.FindBanByChiNhanhId(toChiNhanh.Id.Value);
            _ChiNhanh_BanChiNhanh_ListBanChiNhanh.Clear();
            foreach (BanChiNhanh banChiNhanh in listBanChiNhanh)
            {
                _ChiNhanh_BanChiNhanh_ListBanChiNhanh.Add(banChiNhanh);
            }
            _Main_DialogManager.CreateMessageDialog().SetCaption("Thông báo").SetMessage("Thực hiện thành công").SetImage(DialogManager.DialogImage.Information).ShowDialog();
        }

        private void Button_BanChiNhanh_Duplicate_Back_Click(object sender, RoutedEventArgs e)
        {
            TabItem_BanChiNhanh_Edit.IsSelected = true;
        }

        private void Button_BanChiNhanh_Back_Click(object sender, RoutedEventArgs e)
        {
            TabItem_ChiNhanh.IsSelected = true;
        }
        #endregion
    }
}
