﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanAn.GUI.BoPhanTrucTongDaiGUI
{
    public class KhachHangShow
    {
        int _Ma;
        string _SoDienThoai;
        string _SoCMND;

        public int Ma { get => _Ma; set => _Ma = value; }
        public string SoDienThoai { get => _SoDienThoai; set => _SoDienThoai = value; }
        public string SoCMND { get => _SoCMND; set => _SoCMND = value; }
    }
}
