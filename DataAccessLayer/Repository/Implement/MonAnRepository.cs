﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;
using Dapper;

namespace QuanLyQuanAn.DataAccessLayer
{
    internal class MonAnRepository : RepositoryBase, IMonAnRepository
    {
        public MonAnRepository(IDbTransaction transaction) : base(transaction)
        {

        }

        public bool DeleteMonAn(int id)
        {
            string sql = @"DELETE FROM [MonAn] WHERE Id = @Id;";
            int rowsCount = Connection.Execute(sql: sql, param: new { Id = id }, transaction: Transaction);
            if (rowsCount == 0)
                return false;
            return true;
        }

        public bool DeleteMonAn(MonAn monAn)
        {
            if (monAn == null || !monAn.Id.HasValue)
                throw new ArgumentNullException("monAn");
            return DeleteMonAn(monAn.Id.Value);
        }

        public IEnumerable<MonAn> GetByDanhMucId(int? danhMucId)
        {
            string sql;
            if (danhMucId == null)
                sql = @"SELECT * FROM [MonAn] WHERE DanhMucId IS NULL ORDER BY TenMonAn ASC;";
            else
                sql = @"SELECT * FROM [MonAn] WHERE DanhMucId = @DanhMucId ORDER BY TenMonAn ASC;";
            var queryResult = Connection.Query<MonAn>(sql: sql, param: new { DanhMucId = danhMucId }, transaction: Transaction);
            return queryResult;
        }

        public IEnumerable<MonAn> GetAll()
        {
            string sql = @"SELECT * FROM [MonAn] ORDER BY TenMonAn ASC;";
            var queryResult = Connection.Query<MonAn>(sql: sql, param: null, transaction: Transaction);
            return queryResult;
        }

        public MonAn GetMonAn(int id)
        {
            string sql = @"SELECT * FROM [MonAn] WHERE Id = @Id;";
            return Connection.QuerySingleOrDefault<MonAn>(sql: sql, param: new { Id = id }, transaction: Transaction);
        }

        public MonAn GetMonAn(MonAn monAn)
        {
            if (monAn == null || !monAn.Id.HasValue)
                return null;
            return GetMonAn(monAn.Id.Value);
        }

        public bool InsertMonAn(MonAn monAn)
        {
            string sql = @"INSERT INTO [MonAn] (TenMonAn, DanhMucId, MoTa, HinhAnh, GiaThamKhao) VALUES (@TenMonAn, @DanhMucId, @MoTa, @HinhAnh, @GiaThamKhao); SELECT SCOPE_IDENTITY()";
            try
            {
                monAn.Id = Connection.ExecuteScalar<int>(sql: sql, param: monAn, transaction: Transaction);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateMonAn(MonAn monAn)
        {
            string sql = @"UPDATE [MonAn] SET TenMonAn = @TenMonAn, DanhMucId = @DanhMucId, MoTa = @MoTa, HinhAnh = @HinhAnh, GiaThamKhao = @GiaThamKhao, ThoiGianCapNhat = @ThoiGianCapNhat WHERE Id = @Id;";
            monAn.ThoiGianCapNhat = DateTime.Now;
            try
            {
                int rowsCount = Connection.Execute(sql: sql, param: monAn, transaction: Transaction);
                if (rowsCount == 0)
                    return false;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<int> GetAllMonAnIdInKhongThongKe()
        {
            string sql = @"SELECT * FROM [KhongThongKe];";
            return Connection.Query<int>(sql: sql, param: null, transaction: Transaction);
        }

        public bool InsertMonAnIntoKhongThongKe(int monAnId)
        {
            string sql = "INSERT INTO [KhongThongKe] (MonAnId) VALUES (@MonAnId);";
            try
            {
                Connection.Execute(sql: sql, param: new { MonAnId = monAnId }, transaction: Transaction);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool IsMonAnIdInKhongThongKe(int monAnId)
        {
            string sql = @"USP_KiemTraMonAnTonTaiTrongKhongThongKe";

            DynamicParameters parameters = new DynamicParameters();
            parameters.Add(name: "@MonAnId", value: monAnId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            parameters.Add(name: "@Result", dbType: DbType.Int32, direction: ParameterDirection.Output);
            parameters.Add(name: "@ReturnValue", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

            Connection.Execute(sql: sql, param: parameters, transaction: Transaction, commandType: CommandType.StoredProcedure);

            int ketQua = parameters.Get<int>("@Result");
            int returnValue = parameters.Get<int>("@ReturnValue");

            if (returnValue != 0)
            {
                throw new Exception("MonAnId is null.");
            }
            if (ketQua == 0)
                return false;

            return true;
        }

        public bool DeleteMonAnIdFromKhongThongKe(int monAnId)
        {
            string sql = @"DELETE FROM [KhongThongKe] WHERE MonAnId = @MonAnId;";
            int rowsCount = Connection.Execute(sql: sql, param: new { MonAnId = monAnId }, transaction: Transaction);
            if (rowsCount == 1)
                return true;
            return false;
        }
    }
}
