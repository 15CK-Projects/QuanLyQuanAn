﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using QuanLyQuanAn.DataObject;
using QuanLyQuanAn.DataAccessLayer;
using QuanLyQuanAn.Common;

namespace QuanLyQuanAn.Service
{
    public class DanhMucService : IDanhMucService
    {
        public bool DeleteDanhMuc(int id)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                var listMonAn = uow.MonAnRepository.GetByDanhMucId(id);
                foreach (MonAn monAn in listMonAn)
                {
                    monAn.DanhMucId = null; // null is Uncategory.
                    if (!uow.MonAnRepository.UpdateMonAn(monAn))
                    {
                        return false;
                    }
                }

                if (!uow.DanhMucRepository.DeleteDanhMuc(id))
                    return false;

                uow.Commit();
                return true;
            }            
        }

        public IEnumerable<DanhMuc> GetAllDanhMuc()
        {
            IEnumerable<DanhMuc> listDanhMuc = null;
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                listDanhMuc = uow.DanhMucRepository.GetAll();
                uow.Commit();
            }

            foreach (DanhMuc danhMuc in listDanhMuc)
            {
                // Điều chỉnh hình ảnh trước khi trả về kết quả cho GUI.
                var imageName = danhMuc.HinhAnh;
                HandleImagePathWhenLoad(ref imageName);
                danhMuc.HinhAnh = imageName;
            }
            // Thứ tự lúc đọc từ database là theo Alphabet.
            var finalListDanhMuc = listDanhMuc.Concat(new DanhMuc[] { DanhMucHeThong.ChuaPhanLoai, DanhMucHeThong.TatCa, DanhMucHeThong.Top10 });
            return finalListDanhMuc;
        }

        public bool InsertDanhMuc(DanhMuc danhMuc)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                var imagePath = danhMuc.HinhAnh;
                HandleImagePathWhenWrite(ref imagePath);
                danhMuc.HinhAnh = imagePath;

                if (!uow.DanhMucRepository.InsertDanhMuc(danhMuc))
                    return false;

                uow.Commit();
                return true;
            }
        }

        public bool UpdateDanhMuc(DanhMuc danhMuc)
        {
            // Không được Update những danh mục "mềm".
            if (danhMuc.Id == DanhMucHeThong.ChuaPhanLoai.Id)
                return false;
            if (danhMuc.Id == DanhMucHeThong.TatCa.Id)
                return false;
            if (danhMuc.Id == DanhMucHeThong.Top10.Id)
                return false;

            var imagePath = danhMuc.HinhAnh;
            HandleImagePathWhenWrite(ref imagePath);
            danhMuc.HinhAnh = imagePath;

            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                if (!uow.DanhMucRepository.UpdateDanhMuc(danhMuc))
                    return false;
                uow.Commit();
                return true;
            }
        }

        /// <summary>
        /// Điều chỉnh lại tên file ảnh và lưu trữ file ảnh nếu cần thiết.
        /// </summary>
        /// <param name="inputImagePath"></param>
        private void HandleImagePathWhenWrite(ref string inputImagePath)
        {
            // File ảnh: No-Image.png là file ảnh mặc định. Luôn luôn tồn tại file này trong thư mục Image.

            // Nếu đường dẫn null (hoặc trống) để là null.
            if (string.IsNullOrWhiteSpace(inputImagePath))
            {
                inputImagePath = null;
                return;
            }
            
            // Nếu tên ảnh là "No-Image.png" thì chuyển thành null.
            if (inputImagePath == "No-Image.png")
            {
                inputImagePath = null;
                return;
            }

            // Nếu tên ảnh không phải là "No-Images.png" => Đã có thiết đặt phần ảnh.
            // ImagePath lúc này là đường dẫn, không phải tên file lưu trong database.
            if (inputImagePath != "No-Image.png" && !File.Exists(string.Format(@"Images\{0}", Path.GetFileName(inputImagePath))))
            {
                // Nếu file ảnh tồn tại.
                if (File.Exists(inputImagePath))
                {
                    string newName = string.Format("IMG {0}{1}", DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-ffff"), Path.GetExtension(inputImagePath));
                    try
                    {
                        File.Copy(inputImagePath, string.Format(@"Images\{0}", newName));
                        inputImagePath = newName;
                    }
                    catch
                    {
                        inputImagePath = "No-Image.png";
                    }
                }
                else
                {
                    inputImagePath = "No-Image.png";
                }
            }
        }

        /// <summary>
        /// Điều chỉnh lại tên file ảnh trong một số trường hợp đặc biệt.
        /// </summary>
        /// <param name="inputImagePath"></param>
        private void HandleImagePathWhenLoad(ref string inputImagePath)
        {
            if (inputImagePath == null)
            {
                inputImagePath = "No-Image.png";
                return;
            }

            if (!File.Exists(string.Format(@"Images\{0}", Path.GetFileName(inputImagePath))))
            {
                inputImagePath = "No-Image.png";
            }
        }
    }
}
