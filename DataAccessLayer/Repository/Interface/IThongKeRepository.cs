﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;

namespace QuanLyQuanAn.DataAccessLayer
{
    public interface IThongKeRepository
    {
        IEnumerable<MonAn> GetTop10MonAnBanChayTatCaChiNhanh(DateTime from, DateTime to);
    }
}
