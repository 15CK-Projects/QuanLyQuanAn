﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;
using Dapper;

namespace QuanLyQuanAn.DataAccessLayer
{
    internal class TaiKhoanRepository : RepositoryBase, ITaiKhoanRepository
    {
        public TaiKhoanRepository(IDbTransaction transaction) : base(transaction)
        {

        }

        public bool DeleteTaiKhoan(int id)
        {
            string sql = @"DELETE FROM TaiKhoan WHERE Id = @Id;";
            int rowsCount = Connection.Execute(sql: sql, param: new { Id = id }, transaction: Transaction);
            if (rowsCount == 0)
            {
                return false;
            }
            return true;                
        }

        public bool DeleteTaiKhoan(TaiKhoan taiKhoan)
        {
            if (taiKhoan == null || !taiKhoan.Id.HasValue)
            {
                throw new ArgumentNullException("taiKhoan");
            }

            return DeleteTaiKhoan(taiKhoan.Id.Value);
        }

        public IEnumerable<TaiKhoan> GetAll()
        {
            string sql = @"SELECT * FROM TaiKhoan;";
            var queryResult = Connection.Query<TaiKhoan>(sql: sql, param: null, transaction: Transaction);
            return queryResult;
        }

        public TaiKhoan GetTaiKhoan(int id)
        {
            string sql = @"SELECT * FROM TaiKhoan WHERE Id = @id";
            var queryResult = Connection.QuerySingleOrDefault(sql: sql, param: new { Id = id }, transaction: Transaction);
            return queryResult;
        }

        public TaiKhoan GetTaiKhoan(TaiKhoan taiKhoan)
        {
            if (taiKhoan == null || !taiKhoan.Id.HasValue)
            {
                throw new ArgumentNullException("taiKhoan");
            }
            return GetTaiKhoan(taiKhoan.Id.Value);
        }

        public TaiKhoan GetTaiKhoanByUserName(string userName)
        {
            string sql = @"SELECT * FROM [TaiKhoan] WHERE [UserName] = @UserName;";
            return Connection.QuerySingleOrDefault<TaiKhoan>(sql: sql, param: new { UserName = userName }, transaction: Transaction);
        }

        public bool UpdateTaiKhoan(TaiKhoan taiKhoan)
        {
            string sql = @"UPDATE TaiKhoan SET UserName = @UserName, Password = @Password, NhomTaiKhoanId = @NhomTaiKhoanId, IsLocked = @IsLocked, NhanVienId = @NhanVienId, ThoiGianCapNhat = @ThoiGianCapNhat WHERE Id = @Id;";
            taiKhoan.ThoiGianCapNhat = DateTime.Now;
            int rowsCount = Connection.Execute(sql: sql, param: taiKhoan, transaction: Transaction);
            if (rowsCount == 0)
            {
                return false;
            }
            return true;
        }
    }
}
