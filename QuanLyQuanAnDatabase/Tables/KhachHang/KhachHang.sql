﻿CREATE TABLE KhachHang
(
	Id					int,
	SoDienThoai			nvarchar(255),
	GioiTinh			nvarchar(255),
	NgaySinh			date,
	SoCmnd				nvarchar(255),
	DiaChi				nvarchar(255),
	ThoiGianTao			datetime DEFAULT GETDATE(),
	ThoiGianCapNhat		datetime DEFAULT GETDATE(),

	CONSTRAINT PK_KhachHang PRIMARY KEY (Id),
	CONSTRAINT AK_KhachHang	UNIQUE (SoCmnd) 
);
