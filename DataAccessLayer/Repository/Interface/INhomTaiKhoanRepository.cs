﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;

namespace QuanLyQuanAn.DataAccessLayer
{
    public interface INhomTaiKhoanRepository
    {
        bool AddNhomTaiKhoan(NhomTaiKhoan nhom);
        IEnumerable<NhomTaiKhoan> GetAll();
        NhomTaiKhoan GetNhomTaiKhoan(int id);
        NhomTaiKhoan GetNhomTaiKhoan(NhomTaiKhoan nhom);
        bool DeleteNhomTaiKhoan(int id);
        bool DeleteNhomTaiKhoan(NhomTaiKhoan nhom);
        bool UpdateNhomTaiKhoan(NhomTaiKhoan nhom);
    }
}
