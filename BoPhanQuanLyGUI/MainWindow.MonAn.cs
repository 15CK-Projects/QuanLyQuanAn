﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.ComponentModel;
using QuanLyQuanAn.Service;
using QuanLyQuanAn.DataObject;
using QuanLyQuanAn.Common;

namespace QuanLyQuanAn.GUI.BoPhanQuanLyGUI
{
    public partial class MainWindow : Window
    {
        private ObservableCollection<DanhMuc> _MonAn_ListDanhMuc = new ObservableCollection<DanhMuc>();
        private ObservableCollection<MonAn> _MonAn_ListMonAn = new ObservableCollection<MonAn>();

        private void Button_MonAn_Click(object sender, RoutedEventArgs e)
        {
            var listDanhMuc = _Main_DanhMucService.GetAllDanhMuc();
            _MonAn_ListDanhMuc.Clear();
            foreach (DanhMuc danhMuc in listDanhMuc)
            {
                _MonAn_ListDanhMuc.Add(danhMuc);
            }

            ComboBox_DanhMuc.ItemsSource = _MonAn_ListDanhMuc;
            var listDanhMucWithoutAllCategory = _MonAn_ListDanhMuc.Where(o => o.Id != DanhMucHeThong.TatCa.Id && o.Id != DanhMucHeThong.Top10.Id);
            ComboBox_DanhMucInDetails.ItemsSource = listDanhMucWithoutAllCategory;

            TabItem_MonAn.IsSelected = true;
        }

        private void ComboBox_DanhMuc_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _MonAn_ListMonAn.Clear();

            DanhMuc selectedDanhMuc = ComboBox_DanhMuc.SelectedItem as DanhMuc;
            if (selectedDanhMuc == null)
            {
                return;
            }

            IEnumerable<MonAn> listMonAn = _Main_MonAnService.GetMonAnByDanhMucIdIncludedDefinedDanhMuc(selectedDanhMuc.Id);

            foreach (MonAn monAn in listMonAn)
            {
                _MonAn_ListMonAn.Add(monAn);
            }

            DataGrid_MonAn.ItemsSource = _MonAn_ListMonAn;
        }

        private void DataGrid_MonAn_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MonAn selectedMonAn = DataGrid_MonAn.SelectedItem as MonAn;
            if (selectedMonAn == null)
                return;

            TextBox_TenMonAn.Text = selectedMonAn.TenMonAn;
            ComboBox_DanhMucInDetails.SelectedItem = _MonAn_ListDanhMuc.Where(o => o.Id == selectedMonAn.DanhMucId).FirstOrDefault();
            TextBox_MoTaMonAn.Text = selectedMonAn.MoTa;
            TextBox_GiaThamKhao.Text = string.Format("{0:N0}", selectedMonAn.GiaThamKhao ?? 0M);
            CheckBox_KhongThongKe.IsChecked = selectedMonAn.KhongThongKe;

            Image_MonAn.Tag = selectedMonAn.HinhAnh;
            var imagePath = string.Format(@"Images\{0}", selectedMonAn.HinhAnh);
            if (System.IO.File.Exists(imagePath))
            {
                Image_MonAn.Source = new BitmapImage(new Uri(System.IO.Path.GetFullPath(imagePath)));
            }
        }

        private void Button_AddMonAn_Click(object sender, RoutedEventArgs e)
        {
            MonAn newMonAn = new MonAn();
            newMonAn.TenMonAn = TextBox_TenMonAn.Text;
            DanhMuc selectedDanhMuc = ComboBox_DanhMucInDetails.SelectedItem as DanhMuc;
            if (selectedDanhMuc != null)
            {
                newMonAn.DanhMucId = selectedDanhMuc.Id;
            }
            else
            {
                newMonAn.DanhMucId = DanhMucHeThong.ChuaPhanLoai.Id;
            }

            newMonAn.MoTa = TextBox_MoTaMonAn.Text;
            decimal giaThamKhao;
            if (!decimal.TryParse(TextBox_GiaThamKhao.Text, System.Globalization.NumberStyles.AllowThousands, System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), out giaThamKhao))
            {
                giaThamKhao = 0M;
            }
            newMonAn.GiaThamKhao = giaThamKhao;
            newMonAn.HinhAnh = Image_MonAn.Tag as string;

            newMonAn.KhongThongKe = CheckBox_KhongThongKe.IsChecked;

            if (_Main_MonAnService.InsertMonAn(newMonAn))
            {
                if (ComboBox_DanhMucInDetails.SelectedItem == ComboBox_DanhMuc.SelectedItem)
                {
                    _MonAn_ListMonAn.Add(newMonAn);
                }
                _Main_DialogManager.CreateMessageDialog().SetCaption("Thông báo").SetMessage("Đã thêm món ăn thành công!").SetImage(DialogManager.DialogImage.Information).ShowDialog();
            }
            else
            {
                _Main_DialogManager.CreateMessageDialog().SetCaption("Thông báo").SetMessage("Không thêm được món ăn!").SetImage(DialogManager.DialogImage.Error).ShowDialog();
            }
        }

        private void Button_UpdateMonAn_Click(object sender, RoutedEventArgs e)
        {
            MonAn selectedMonAn = DataGrid_MonAn.SelectedItem as MonAn;
            if (selectedMonAn == null)
            {
                return;
            }

            // Old values.
            string tenMonAn = selectedMonAn.TenMonAn;
            int? danhMucId = selectedMonAn.DanhMucId;
            string moTa = selectedMonAn.MoTa;
            decimal? giaThamKhao = selectedMonAn.GiaThamKhao;
            string hinhAnh = selectedMonAn.HinhAnh;
            bool? khongThongKe = selectedMonAn.KhongThongKe;

            // Update the object with new values.
            selectedMonAn.TenMonAn = TextBox_TenMonAn.Text;

            DanhMuc selectedDanhMuc = ComboBox_DanhMucInDetails.SelectedItem as DanhMuc;
            if (selectedDanhMuc == null)
                selectedMonAn.DanhMucId = DanhMucHeThong.ChuaPhanLoai.Id;
            else
                selectedMonAn.DanhMucId = selectedDanhMuc.Id;

            selectedMonAn.MoTa = TextBox_MoTaMonAn.Text;
            decimal newGiaThamKhao;
            if (!decimal.TryParse(TextBox_GiaThamKhao.Text, System.Globalization.NumberStyles.AllowThousands, System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), out newGiaThamKhao))
            {
                giaThamKhao = 0M;
            }
            selectedMonAn.GiaThamKhao = newGiaThamKhao;
            selectedMonAn.HinhAnh = Image_MonAn.Tag as string;
            if (CheckBox_KhongThongKe.IsChecked == true)
                selectedMonAn.KhongThongKe = true;
            else
                selectedMonAn.KhongThongKe = false;

            if (_Main_MonAnService.UpdateMonAn(selectedMonAn))
            {
                DanhMuc currentDanhMucFilter = ComboBox_DanhMuc.SelectedItem as DanhMuc;
                if (ComboBox_DanhMuc.SelectedItem != ComboBox_DanhMucInDetails.SelectedItem &&
                    currentDanhMucFilter.Id != DanhMucHeThong.TatCa.Id &&
                    currentDanhMucFilter.Id != DanhMucHeThong.Top10.Id)
                    _MonAn_ListMonAn.Remove(selectedMonAn);
                else
                    DataGrid_MonAn.Items.Refresh();
                _Main_DialogManager.CreateMessageDialog().SetCaption("Thông báo").SetMessage("Đã cập nhật món ăn thành công!").SetImage(DialogManager.DialogImage.Information).ShowDialog();
            }
            else
            {
                // Restore old values.
                selectedMonAn.TenMonAn = tenMonAn;
                selectedMonAn.DanhMucId = danhMucId;
                selectedMonAn.MoTa = moTa;
                selectedMonAn.GiaThamKhao = giaThamKhao;
                selectedMonAn.HinhAnh = hinhAnh;
                selectedMonAn.KhongThongKe = khongThongKe;
                _Main_DialogManager.CreateMessageDialog().SetCaption("Thông báo").SetMessage("Không thể cập nhật món ăn!").SetImage(DialogManager.DialogImage.Error).ShowDialog();
            }
        }

        private void Button_DeleteMonAn_Click(object sender, RoutedEventArgs e)
        {
            MonAn selectedMonAn = DataGrid_MonAn.SelectedItem as MonAn;
            if (selectedMonAn == null)
            {
                return;
            }

            var dialog = _Main_DialogManager.CreateMessageDialog()
                .SetButton(DialogManager.DialogButton.YesNo)
                .SetImage(DialogManager.DialogImage.Question)
                .SetMessage(string.Format("Bạn có muốn xóa món ăn '{0}' không?", selectedMonAn.TenMonAn))
                .SetCaption("Xác nhận xóa danh mục");
            dialog.ShowDialog();
            if (dialog.MessageDialogResult == DialogManager.DialogResult.Yes)
            {
                if (_Main_MonAnService.DeleteMonAn(selectedMonAn.Id.Value))
                {
                    _MonAn_ListMonAn.Remove(selectedMonAn);
                }
                else
                {
                    _Main_DialogManager.CreateMessageDialog().SetCaption("Thông báo").SetMessage("Không xóa được món ăn!").SetImage(DialogManager.DialogImage.Error).ShowDialog();
                }
            }
        }

        private void Button_BrowseMonAnImage_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            dialog.Title = "Tìm hình ảnh cho món ăn";
            dialog.Filter = "Image Files (*.jpg; *.png)|*.jpg;*.png|JPEG files (*.jpg)|*.jpg|PNG Files (*.png)|*.png";
            dialog.Multiselect = false;
            bool? result = dialog.ShowDialog();
            if (result == true)
            {
                Image_MonAn.Tag = dialog.FileName;
                Image_MonAn.Source = new BitmapImage(new Uri(dialog.FileName));
            }
        }

        private void Button_DeleteMonAnImage_Click(object sender, RoutedEventArgs e)
        {
            string imagePath = string.Format(@"Images\{0}", "No-Image.png");
            Image_MonAn.Tag = null;
            if (System.IO.File.Exists(imagePath))
            {
                Image_MonAn.Source = new BitmapImage(new Uri(System.IO.Path.GetFullPath(imagePath)));
            }
        }
    }
}
