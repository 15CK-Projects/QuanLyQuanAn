﻿ALTER TABLE [dbo].[ChiTietDonHang]
	ADD CONSTRAINT [FK_ChiTietDonHang_MonAn]
	FOREIGN KEY (MonAnId)
	REFERENCES [MonAn] ([Id])
