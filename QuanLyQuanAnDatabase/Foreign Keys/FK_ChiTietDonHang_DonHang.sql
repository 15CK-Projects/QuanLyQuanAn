﻿ALTER TABLE [dbo].[ChiTietDonHang]
	ADD CONSTRAINT [FK_ChiTietDonHang_DonHang]
	FOREIGN KEY (DonHangId)
	REFERENCES [DonHang] (Id)
