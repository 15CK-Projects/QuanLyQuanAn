﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;
using QuanLyQuanAn.DataAccessLayer;

namespace QuanLyQuanAn.Service
{
    public class TaiKhoanService : ITaiKhoanService
    {
        public TaiKhoan Login(string userName, string password, out bool result)
        {
            var found = GetTaiKhoanByUserName(userName);
            if (found == null)
            {
                result = false;
                return null;
            }

            if (found.Password != password)
            {
                result = false;
                return null;
            }

            WatchDog.InitializePermissionsForNewSession(found.NhomTaiKhoanId);
            WatchDog.CurrentUser = found;

            result = true;
            return found;
        }

        public TaiKhoan GetTaiKhoan(int id)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                var result = uow.TaiKhoanRepository.GetTaiKhoan(id);
                uow.Commit();
                return result;
            }
        }

        public TaiKhoan GetTaiKhoanByUserName(string userName)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                var found = uow.TaiKhoanRepository.GetTaiKhoanByUserName(userName);
                uow.Commit();
                return found;
            }
        }
    }
}
