﻿CREATE TABLE [dbo].[ChiNhanh]
(
	Id					int IDENTITY(1,1),
	TenChiNhanh			nvarchar(255),
	SoDienThoai			nvarchar(255),
	DiaChi				nvarchar(255),
	ThoiGianTao			datetime DEFAULT GETDATE(),
	ThoiGianCapNhat		datetime DEFAULT GETDATE(),

	CONSTRAINT PK_ChiNhanh PRIMARY KEY (Id)
);
