﻿using QuanLyQuanAn.GUI.BoPhanQuanLyGUI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanAn.GUI.BoPhanTrucTongDaiGUI
{
    public class KhachHang
    {
        string _SoDienThoai;

        public string SoDienThoai { get => _SoDienThoai; set => _SoDienThoai = value; }
        public string GioiTinh { get => _GioiTinh; set => _GioiTinh = value; }
        public DateTime NgaySinh { get => _NgaySinh; set => _NgaySinh = value; }
        public string SoCMND { get => _SoCMND; set => _SoCMND = value; }
        public string DiaChiMuc1 { get => _DiaChiMuc1; set => _DiaChiMuc1 = value; }
        public string DiaChiMuc2 { get => _DiaChiMuc2; set => _DiaChiMuc2 = value; }
        public string DiaChiMuc3 { get => _DiaChiMuc3; set => _DiaChiMuc3 = value; }
        public DateTime ThoiGianCapNhat { get => _ThoiGianCapNhat; set => _ThoiGianCapNhat = value; }
        public DateTime ThoiGianTao { get => _ThoiGianTao; set => _ThoiGianTao = value; }
        public int Ma { get => _Ma; set => _Ma = value; }

        int _Ma;

        string _GioiTinh;

        DateTime _NgaySinh;

        string _SoCMND;

        string _DiaChiMuc1;

        string _DiaChiMuc2;

        string _DiaChiMuc3;

        DateTime _ThoiGianTao;

        DateTime _ThoiGianCapNhat;

        public void khachhang()
        {
            _SoDienThoai = "";
            _SoCMND = "";
            _GioiTinh = "";
            _NgaySinh = DateTime.Now;
            _DiaChiMuc1 = "";
            _DiaChiMuc2 = "";
            _DiaChiMuc3 = "";
            ThoiGianTao = DateTime.Now;
            _ThoiGianCapNhat = DateTime.Now;
        }

        public bool ktrathongtin(KhachHang kh)
        {
            Provider provider = new Provider();

            var sql = "select * from KhachHang kh where kh.SoCMND = " + kh.SoCMND.ToString();

            var Khachhang = provider.Select(System.Data.CommandType.Text, sql);

            if (Khachhang.Rows.Count > 0)
            {
                return true;
            }
            return false;

        }

        public bool ThemKhachHang(KhachHang khachhang)
        {
            Provider provider = new Provider();

            var sql = "insert into kh value sodienthoai = " + khachhang.SoDienThoai + "cmns =" + khachhang.SoCMND + "gt =" + khachhang.GioiTinh + "ns =" +khachhang.NgaySinh + "dcm1 =" +khachhang._DiaChiMuc1 + "dcm2 =" +khachhang._DiaChiMuc2 + "dcm3 =" +khachhang._DiaChiMuc3 + "tgt =" +khachhang.ThoiGianTao + "tgcn =" +khachhang._ThoiGianCapNhat;
            var nRow = provider.ExecuteNonQuery(System.Data.CommandType.Text, sql);

            if(nRow > 0)
            {
                return true;
            }
            return false;
        }

        public bool Them(KhachHang kh)
        {
            if (!ktrathongtin(kh))
            {
                return ThemKhachHang(kh);
            }
            return false;
        }

        public SqlDataReader InsertStore(KhachHang kh)
        {
            Provider pro = new Provider();
            using (SqlConnection conn = new SqlConnection(pro.ConnectionString))
            {
                conn.Open();

                // 1.  create a command object identifying the stored procedure
                SqlCommand cmd = new SqlCommand("insertKhachHang", conn);

                // 2. set the command object so it knows to execute a stored procedure
                cmd.CommandType = CommandType.StoredProcedure;

                // 3. add parameter to command, which will be passed to the stored procedure
                cmd.Parameters.Add(new SqlParameter("@SoCMND", kh.SoCMND));


                // execute the command
                return cmd.ExecuteReader();
            }
        }
    }
}
