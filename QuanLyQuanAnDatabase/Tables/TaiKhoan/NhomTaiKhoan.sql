﻿CREATE TABLE [dbo].[NhomTaiKhoan]
(
	Id					int IDENTITY(1,1),
	TenNhom				nvarchar(255),
	QuyenNhom			ntext,
	NhomChaId			int DEFAULT NULL,
	ThoiGianTao			datetime DEFAULT GETDATE(),
	ThoiGianCapNhat		datetime DEFAULT GETDATE(),

	CONSTRAINT PK_NhomTaiKhoan PRIMARY KEY (Id)
);
