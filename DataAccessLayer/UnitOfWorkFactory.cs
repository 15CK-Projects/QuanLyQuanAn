﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanAn.DataAccessLayer
{
    public static class UnitOfWorkFactory
    {
        private static Func<IUnitOfWork> _factoryMethod;
        internal static Func<IUnitOfWork> ClearAssigment = () => null;

        static UnitOfWorkFactory()
        {
            _factoryMethod = FactoryMethod.UnitOfWorkFactoryMethod;
        }

        public static IUnitOfWork Create()
        {
            if (_factoryMethod == null)
            {
                throw new InvalidOperationException("You must call SetFactoryMethod() first.");
            }

            return _factoryMethod();
        }

        public static void SetFactoryMethod(Func<IUnitOfWork> factoryMethod)
        {
            if (factoryMethod == null)
            {
                throw new ArgumentNullException("factoryMethod");
            }

            _factoryMethod = ClearAssigment == factoryMethod ? null : factoryMethod;
        }
    }
}
