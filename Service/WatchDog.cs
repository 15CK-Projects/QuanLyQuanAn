﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;

namespace QuanLyQuanAn.Service
{
    public static class WatchDog
    {

        private static readonly List<string> _permissions;
        private static readonly List<string> _negatedPermissions;

        private static INhomTaiKhoanService _nhomTaiKhoanService;

        private static TaiKhoan _currentUser;
        public static TaiKhoan CurrentUser { get => _currentUser; set => _currentUser = value; }

        static WatchDog()
        {
            _permissions = new List<string>();
            _negatedPermissions = new List<string>();
            _nhomTaiKhoanService = new NhomTaiKhoanService();

            _currentUser = null;
        }

        public static void InitializePermissionsForNewSession(int? nhomTaiKhoanId)
        {
            _permissions.Clear();
            _negatedPermissions.Clear();

            HashSet<string> permissions = null;
            HashSet<string> negatedPermissions = null;
            List<int> reversed = null;

            NhomTaiKhoan nhomTaiKhoan = null;
            if (nhomTaiKhoanId == null)
                permissions = new HashSet<string>(Permissions.DefaultPermissions);
            else
            {
                nhomTaiKhoan = _nhomTaiKhoanService.GetNhomTaiKhoan(nhomTaiKhoanId.Value);
                permissions = new HashSet<string>();
                negatedPermissions = new HashSet<string>();
                reversed = new List<int>();
            }

            while (nhomTaiKhoan != null)
            {
                if (!nhomTaiKhoan.Id.HasValue)
                    break;
                if (reversed.Contains(nhomTaiKhoan.Id.Value))
                    break;

                string permissionsString = (nhomTaiKhoan.QuyenNhom ?? string.Empty).Trim();
                string[] perms = permissionsString.Split(';');
                foreach (string perm in perms)
                {
                    if (string.IsNullOrWhiteSpace(perm))
                        continue;

                    string p = perm.Trim();
                    if (!p.StartsWith("!"))
                        permissions.Add(p);
                    else
                        negatedPermissions.Add(p.Substring(1));
                }

                reversed.Add(nhomTaiKhoan.Id.Value);

                if (nhomTaiKhoan.NhomChaId == null)
                    break;
                nhomTaiKhoan = _nhomTaiKhoanService.GetNhomTaiKhoan(nhomTaiKhoan.NhomChaId.Value);
            }

            _permissions.AddRange(permissions);
            _negatedPermissions.AddRange(negatedPermissions);
        }

        public static bool HasPermission(string permission)
        {
            if (string.IsNullOrWhiteSpace(permission))
                return false;

            if (_negatedPermissions.Contains(permission))
                return false;

            if (!_permissions.Contains(permission))
                return false;

            return true;
        }
    }
}
