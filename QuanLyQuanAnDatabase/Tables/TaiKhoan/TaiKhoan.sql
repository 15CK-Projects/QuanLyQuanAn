﻿CREATE TABLE [dbo].[TaiKhoan]
(
	[Id]				int IDENTITY(1,1),
	[UserName]			nvarchar(255) NOT NULL,
	[Password]			nvarchar(255),
	[NhomTaiKhoanId]	int,
	IsLocked			bit DEFAULT 0,
	NhanVienId			int,
	ThoiGianTao			datetime DEFAULT GETDATE(),
	ThoiGianCapNhat		datetime DEFAULT GETDATE(),

	CONSTRAINT PK_TaiKhoan PRIMARY KEY (Id),
	CONSTRAINT AK_TaiKhoan UNIQUE (UserName)
);
