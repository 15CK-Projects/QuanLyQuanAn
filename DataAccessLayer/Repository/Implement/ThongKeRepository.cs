﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;
using Dapper;

namespace QuanLyQuanAn.DataAccessLayer
{
    internal class ThongKeRepository : RepositoryBase, IThongKeRepository
    {
        public ThongKeRepository(IDbTransaction transaction) : base(transaction)
        {
        }

        public IEnumerable<MonAn> GetTop10MonAnBanChayTatCaChiNhanh(DateTime from, DateTime to)
        {
            string sql = "[dbo].[USP_Top10MonAnBanChayTatCaChiNhanh]";
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add(name: "@from", value: from, direction: ParameterDirection.Input);
            parameters.Add(name: "@to", value: to, direction: ParameterDirection.Input);
            var result = Connection.Query<MonAn>(sql: sql, param: parameters, transaction: Transaction, commandType: CommandType.StoredProcedure);
            return result;
        }
    }
}
