﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;

namespace QuanLyQuanAn.DataAccessLayer
{
    public interface INhanVienRepository
    {
        NhanVien GetNhanVien(int id);

        bool InsertNhanVien(NhanVien nhanVien);
        bool DeleteNhanVien(int id);
        bool UpdateNhanVien(NhanVien nhanVien);
    }
}
