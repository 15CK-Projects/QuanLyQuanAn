﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.ComponentModel;
using QuanLyQuanAn.Service;
using QuanLyQuanAn.DataObject;
using QuanLyQuanAn.Common;

namespace QuanLyQuanAn.GUI.BoPhanQuanLyGUI
{
    public partial class MainWindow : Window
    {
        private ObservableCollection<DanhMuc> _DanhMuc_ListDanhMuc = new ObservableCollection<DanhMuc>();

        private void Button_DanhMuc_Click(object sender, RoutedEventArgs e)
        {
            var listDanhMuc = _Main_DanhMucService.GetAllDanhMuc();
            _DanhMuc_ListDanhMuc.Clear();

            foreach (DanhMuc danhMuc in listDanhMuc)
            {
                _DanhMuc_ListDanhMuc.Add(danhMuc);
            }

            DataGrid_DanhMuc.ItemsSource = _DanhMuc_ListDanhMuc;
            TabItem_DanhMuc.IsSelected = true;
        }

        private void DataGrid_DanhMuc_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DanhMuc selectedDanhMuc = DataGrid_DanhMuc.SelectedItem as DanhMuc;
            if (selectedDanhMuc == null)
            {
                TextBox_TenDanhMuc.Clear();
                TextBox_MoTaDanhMuc.Clear();
                Image_DanhMuc.ClearValue(Image.SourceProperty);
                Button_DeleteDanhMuc.IsEnabled = false;
                Button_UpdateDanhMuc.IsEnabled = false;
                return;
            }

            // Trường hợp Uncategory, All hoặc Top10 được chọn, đây là 3 danh mục không được phép chỉnh sửa.
            if (selectedDanhMuc.Id == DanhMucHeThong.ChuaPhanLoai.Id ||
                selectedDanhMuc.Id == DanhMucHeThong.TatCa.Id ||
                selectedDanhMuc.Id == DanhMucHeThong.Top10.Id)
            {
                Button_DeleteDanhMuc.IsEnabled = false;
                Button_UpdateDanhMuc.IsEnabled = false;
            }
            else
            {
                Button_DeleteDanhMuc.IsEnabled = true;
                Button_UpdateDanhMuc.IsEnabled = true;
            }

            TextBox_TenDanhMuc.Text = selectedDanhMuc.TenDanhMuc;
            TextBox_MoTaDanhMuc.Text = selectedDanhMuc.MoTa;

            Image_DanhMuc.Tag = selectedDanhMuc.HinhAnh;
            var imagePath = string.Format(@"Images\{0}", selectedDanhMuc.HinhAnh);
            if (System.IO.File.Exists(imagePath))
            {
                Image_DanhMuc.Source = new BitmapImage(new Uri(System.IO.Path.GetFullPath(imagePath)));
            }
        }

        private void Button_BrowseDanhMucImage_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            dialog.Title = "Tìm hình ảnh cho danh mục";
            dialog.Filter = "Image Files (*.jpg; *.png)|*.jpg;*.png|JPEG files (*.jpg)|*.jpg|PNG Files (*.png)|*.png";
            dialog.Multiselect = false;
            bool? result = dialog.ShowDialog();
            if (result == true)
            {
                Image_DanhMuc.Tag = dialog.FileName;
                Image_DanhMuc.Source = new BitmapImage(new Uri(dialog.FileName));
            }
        }

        private void Button_AddDanhMuc_Click(object sender, RoutedEventArgs e)
        {
            DanhMuc danhMuc = new DanhMuc();
            danhMuc.TenDanhMuc = TextBox_TenDanhMuc.Text;
            danhMuc.MoTa = TextBox_MoTaDanhMuc.Text;
            danhMuc.HinhAnh = Image_DanhMuc.Tag as string;
            if (_Main_DanhMucService.InsertDanhMuc(danhMuc))
            {
                _DanhMuc_ListDanhMuc.Add(danhMuc);
                _Main_DialogManager.CreateMessageDialog()
                    .SetCaption("Thông báo")
                    .SetMessage("Đã thêm danh mục thành công!")
                    .SetButton(DialogManager.DialogButton.OK)
                    .SetImage(DialogManager.DialogImage.Information).ShowDialog();
            }
            else
            {
                _Main_DialogManager.CreateMessageDialog()
                    .SetCaption("Thông báo")
                    .SetMessage("Không thêm được danh mục!")
                    .SetButton(DialogManager.DialogButton.OK)
                    .SetImage(DialogManager.DialogImage.Error).ShowDialog();
            }
        }

        private void Button_DeleteDanhMuc_Click(object sender, RoutedEventArgs e)
        {
            DanhMuc selectedDanhMuc = DataGrid_DanhMuc.SelectedItem as DanhMuc;
            if (selectedDanhMuc == null)
            {
                return;
            }

            var dialog = _Main_DialogManager.CreateMessageDialog()
                .SetButton(DialogManager.DialogButton.YesNo)
                .SetImage(DialogManager.DialogImage.Question)
                .SetMessage(string.Format("Bạn có muốn xóa danh mục '{0}' không?", selectedDanhMuc.TenDanhMuc))
                .SetCaption("Xác nhận xóa danh mục");
            dialog.ShowDialog();
            DialogManager.DialogResult dialogResult = dialog.MessageDialogResult;
            if (dialogResult == DialogManager.DialogResult.Yes)
            {
                if (_Main_DanhMucService.DeleteDanhMuc(selectedDanhMuc.Id.Value))
                {
                    _DanhMuc_ListDanhMuc.Remove(selectedDanhMuc);
                }
                else
                {
                    _Main_DialogManager.CreateMessageDialog().SetCaption("Thông báo").SetMessage("Không xóa được danh mục!").SetImage(DialogManager.DialogImage.Error).ShowDialog();
                }
            }
        }

        private void Button_UpdateDanhMuc_Click(object sender, RoutedEventArgs e)
        {
            DanhMuc danhMuc = DataGrid_DanhMuc.SelectedItem as DanhMuc;
            if (danhMuc == null)
            {
                return;
            }

            // Old values.
            string tenDanhMuc = danhMuc.TenDanhMuc;
            string moTaDanhMuc = danhMuc.MoTa;
            string hinhAnhDanhMuc = danhMuc.HinhAnh;

            danhMuc.TenDanhMuc = TextBox_TenDanhMuc.Text;
            danhMuc.MoTa = TextBox_MoTaDanhMuc.Text;
            danhMuc.HinhAnh = Image_DanhMuc.Tag as string;

            if (_Main_DanhMucService.UpdateDanhMuc(danhMuc))
            {
                DataGrid_DanhMuc.Items.Refresh();
                _Main_DialogManager.CreateMessageDialog().SetCaption("Thông báo").SetMessage("Cập nhật danh mục thành công!").SetImage(DialogManager.DialogImage.Information).ShowDialog();
            }
            else
            {
                danhMuc.TenDanhMuc = tenDanhMuc;
                danhMuc.MoTa = moTaDanhMuc;
                danhMuc.HinhAnh = hinhAnhDanhMuc;
                _Main_DialogManager.CreateMessageDialog().SetCaption("Thông báo").SetMessage("Không cập nhật được danh mục!").SetImage(DialogManager.DialogImage.Error).ShowDialog();
            }
        }

        private void Button_DeleteDanhMucImage_Click(object sender, RoutedEventArgs e)
        {

            string imagePath = string.Format(@"Images\{0}", "No-Image.png");
            Image_DanhMuc.Tag = null;
            if (System.IO.File.Exists(imagePath))
            {
                Image_DanhMuc.Source = new BitmapImage(new Uri(System.IO.Path.GetFullPath(imagePath)));
            }
        }

        private void DataGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGridRow row = sender as DataGridRow;
            if (row == null)
            {
                return;
            }

            DanhMuc danhMuc = row.Item as DanhMuc;
            if (danhMuc == null)
            {
                return;
            }

            TabItem_MonAn.IsSelected = true;
        }
    }
}
