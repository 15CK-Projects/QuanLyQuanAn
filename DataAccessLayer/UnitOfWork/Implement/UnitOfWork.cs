﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace QuanLyQuanAn.DataAccessLayer
{
    internal class UnitOfWork : IUnitOfWork
    {
        private IDbConnection _connection;
        private IDbTransaction _transaction;

        private IDanhMucRepository _danhMucRepository;
        private IMonAnRepository _monAnRepository;

        private IChiNhanhRepository _chiNhanhRepository;
        private IDonHangRepository _donHangRepository;
        private IHoaDonRepository _hoaDonRepository;

        private IKhachHangRepository _khachHangRepository;
        private INhanVienRepository _nhanVienRepository;

        private ITaiKhoanRepository _taiKhoanRepository;
        private INhomTaiKhoanRepository _nhomTaiKhoanRepository;

        private IThongKeRepository _thongKeRepository;

        public IDanhMucRepository DanhMucRepository => _danhMucRepository ?? new DanhMucRepository(_transaction);
        public IMonAnRepository MonAnRepository => _monAnRepository ?? new MonAnRepository(_transaction);

        public IChiNhanhRepository ChiNhanhRepository => _chiNhanhRepository ?? new ChiNhanhRepository(_transaction);
        public IDonHangRepository DonHangRepository => _donHangRepository ?? new DonHangRepository(_transaction);
        public IHoaDonRepository HoaDonRepository => _hoaDonRepository ?? new HoaDonRepository(_transaction);

        public IKhachHangRepository KhachHangRepository => _khachHangRepository ?? new KhachHangRepository(_transaction);
        public INhanVienRepository NhanVienRepository => _nhanVienRepository ?? new NhanVienRepository(_transaction);

        public ITaiKhoanRepository TaiKhoanRepository => _taiKhoanRepository ?? new TaiKhoanRepository(_transaction);
        public INhomTaiKhoanRepository NhomTaiKhoanRepository => _nhomTaiKhoanRepository ?? new NhomTaiKhoanRepository(_transaction);

        public IThongKeRepository ThongKeRepository => _thongKeRepository ?? new ThongKeRepository(_transaction);

        public UnitOfWork(IDbConnection connection)
        {
            if (connection == null)
            {
                throw new ArgumentNullException("connection", "Connection is null.");
            }

            if (connection.State != ConnectionState.Closed)
            {
                connection.Close();
            }

            connection.Open();

            _connection = connection;
            _transaction = _connection.BeginTransaction();
        }

        public void Commit()
        {
            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                _transaction.Dispose();
                _transaction = _connection.BeginTransaction();
                ResetRepositories();
            }
        }

        private void ResetRepositories()
        {
            _danhMucRepository = null;
            _monAnRepository = null;

            _chiNhanhRepository = null;
            _donHangRepository = null;
            _hoaDonRepository = null;

            _khachHangRepository = null;
            _nhanVienRepository = null;

            _taiKhoanRepository = null;
            _nhomTaiKhoanRepository = null;

            _thongKeRepository = null;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    if (_transaction != null)
                    {
                        _transaction.Dispose();
                        _transaction = null;
                    }

                    if (_connection != null)
                    {
                        _connection.Dispose();
                        _connection = null;
                    }
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~UnitOfWork() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
