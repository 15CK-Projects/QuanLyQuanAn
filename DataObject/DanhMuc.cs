﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanAn.DataObject
{
    public class DanhMuc
    {
        private int? _id;
        private string _tenDanhMuc;
        private string _moTa;
        private string _hinhAnh;
        private DateTime? _thoiGianTao;
        private DateTime? _thoiGianCapNhat;

        public int? Id { get => _id; set => _id = value; }
        public string TenDanhMuc { get => _tenDanhMuc; set => _tenDanhMuc = value; }
        public string MoTa { get => _moTa; set => _moTa = value; }
        public string HinhAnh { get => _hinhAnh; set => _hinhAnh = value; }
        public DateTime? ThoiGianTao { get => _thoiGianTao; set => _thoiGianTao = value; }
        public DateTime? ThoiGianCapNhat { get => _thoiGianCapNhat; set => _thoiGianCapNhat = value; }
    }
}
