﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanAn.DataAccessLayer
{
    internal static class FactoryMethod
    {
        public static IDbConnection DatabaseConnectionFactoryMethod()
        {
            string providerName = "System.Data.SqlClient";
            string connectionString = "Data Source=.;Initial Catalog=QuanLyQuanAnDatabase;Integrated Security=True";

            DbProviderFactory provider = DbProviderFactories.GetFactory(providerName);
            IDbConnection connection = provider.CreateConnection();

            if (connection == null)
            {
                throw new Exception("Failed to create a connection.");
            }

            connection.ConnectionString = connectionString;
            // Don't open the connection.
            return connection;
        }

        public static IUnitOfWork UnitOfWorkFactoryMethod()
        {
            IDbConnection connection = DatabaseConnectionFactory.Create();
            return new UnitOfWork(connection: connection);
        }
    }
}
