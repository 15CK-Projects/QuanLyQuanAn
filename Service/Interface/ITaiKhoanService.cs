﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;

namespace QuanLyQuanAn.Service
{
    public interface ITaiKhoanService
    {
        TaiKhoan GetTaiKhoan(int id);
        TaiKhoan GetTaiKhoanByUserName(string userName);

        TaiKhoan Login(string userName, string password, out bool result);
    }
}
