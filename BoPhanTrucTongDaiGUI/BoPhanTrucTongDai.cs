﻿using QuanLyQuanAn.GUI.BoPhanQuanLyGUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyQuanAn.GUI.BoPhanTrucTongDaiGUI
{
    public partial class BoPhanTrucTongDai : Form
    {
        public BoPhanTrucTongDai()
        {
            InitializeComponent();
        }

        private void BoPhanTrucTongDai_Load(object sender, EventArgs e)
        {
            string sql = "select * from KhachHang";
            Provider Row = new Provider();
            var DataTable = Row.Select(CommandType.Text, sql);

            var DSKhachHang = new List<KhachHangShow>();

            for (int i = 0; i < DataTable.Rows.Count; i++)
            {
                KhachHangShow KH = new KhachHangShow();
                KH.Ma = (int)DataTable.Rows[i]["MA"];
                KH.SoCMND = (string)DataTable.Rows[i]["SOCMND"];
                KH.SoDienThoai = (string)DataTable.Rows[i]["SODIENTHOAI"];

                DSKhachHang.Add(KH);
            }
            dgvKhachHang.DataSource = DSKhachHang;

        }

        private void dgvKhachHang_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (dgvKhachHang.CurrentCell != null)
            {
                if (dgvKhachHang.Rows[0].Cells[0].Value == null)
                {
                    MessageBox.Show("dữ liệu trống");
                }
                else
                {
                    int g = dgvKhachHang.CurrentCell.RowIndex; // lẤY STT DÒNG ĐANG CHỌN    
                    var _maKhachHang = dgvKhachHang.Rows[g].Cells["Ma"].Value.ToString(); // lẤY MÃ CỦA CỘT ĐANG CHỌN

                    string sql = "select * from DonHang where KhachHangId ='"+ _maKhachHang + "'"; // TRUY VẤN
                    Provider Row = new Provider();
                    var DataTable = Row.Select(CommandType.Text, sql); // KẾT NỐI CSDL -> LẤY DATA

                    var DSDONHANG = new List<DonHang>();

                    for (int i = 0; i < DataTable.Rows.Count; i++)
                    {
                        DonHang DH = new DonHang();
                        DH.NhanVien = (int)DataTable.Rows[i]["NhanVienId"];
                        DH.KhachHang = (int)DataTable.Rows[i]["KhachHangId"];
                        DH.ThoiGianTao = (DateTime)DataTable.Rows[i]["THOIGIANTAO"];
                        DH.ChiNhanh = (int)DataTable.Rows[i]["ChiNhanhId"];
                        DH.BanChiNhanh = (int)DataTable.Rows[i]["BanChiNhanhId"];
                        DH.TongTien = (Decimal)DataTable.Rows[i]["TONGTIEN"];
                        DH.TrangThai = (int)DataTable.Rows[i]["TRANGTHAI"];

                        DSDONHANG.Add(DH); // ADD VÀO LIST
                    }
                    dgvDonHang.DataSource = DSDONHANG;  //LẤY LIST SHOW VÀO DGV
                }
            }  
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            string sql = "select * from KhachHang kh WHERE kh.SoCMND='" + txtTimKiem.Text + "'";
            Provider Row = new Provider();
            var DataTable = Row.Select(CommandType.Text, sql);

            if(DataTable.Rows.Count <= 0)
            {
                // Initializes the variables to pass to the MessageBox.Show method.

                string message = "Tài khoản bạn chưa tồn tại, chọn Yes để thêm, no để hủy";
                string caption = "Tìm kiếm";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result;

                // Displays the MessageBox.

                result = MessageBox.Show(message, caption, buttons);

                if (result == DialogResult.Yes)
                {

                    // Closes the parent form.

                    this.Close();
                    var frmThem = new ThemKhachHang();
                    frmThem.ShowDialog();

                }
                else
                {
                    sql = "select * from KhachHang";
                    Row = new Provider();
                    DataTable = Row.Select(CommandType.Text, sql);

                    var DSKhachHang = new List<KhachHangShow>();

                    for (int i = 0; i < DataTable.Rows.Count; i++)
                    {
                        KhachHangShow KH = new KhachHangShow();
                        KH.Ma = (int)DataTable.Rows[i]["MA"];
                        KH.SoCMND = (string)DataTable.Rows[i]["SOCMND"];
                        KH.SoDienThoai = (string)DataTable.Rows[i]["SODIENTHOAI"];

                        DSKhachHang.Add(KH);
                    }
                    dgvKhachHang.DataSource = DSKhachHang;
                }
            }
            else
            {
                var DSKhachHang = new List<KhachHangShow>();

                for (int i = 0; i < DataTable.Rows.Count; i++)
                {
                    KhachHangShow KH = new KhachHangShow();
                    KH.Ma = (int)DataTable.Rows[i]["MA"];
                    KH.SoCMND = (string)DataTable.Rows[i]["SOCMND"];
                    KH.SoDienThoai = (string)DataTable.Rows[i]["SODIENTHOAI"];

                    DSKhachHang.Add(KH);
                }
                dgvKhachHang.DataSource = DSKhachHang;
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            var frm = new ThemDonHang("Thêm Đơn Hàng");
            frm.ShowDialog();

            
        }

        private void btnSuaDon_Click(object sender, EventArgs e)
        {
            var frm = new ThemDonHang("Sửa Đơn Hàng");
            frm.ShowDialog();
        }

        private void btnXoaDon_Click(object sender, EventArgs e)
        {
            
        }

        private void btnThemKhachHang_Click(object sender, EventArgs e)
        {
            var frmThem = new ThemKhachHang();
            frmThem.ShowDialog();
        }

        private void txtTimKiem_KeyDown(object sender, KeyEventArgs e)
        {
            //
            // Detect the KeyEventArg's key enumerated constant.
            //
            if (e.KeyCode == Keys.Enter)
            {
                MessageBox.Show("You pressed enter! Good job!");
            }
            else if (e.KeyCode == Keys.Escape)
            {
                MessageBox.Show("You pressed escape! What's wrong?");
            }
        }
    }
}
