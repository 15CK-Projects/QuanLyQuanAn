﻿CREATE TABLE [dbo].[ThongKeDoanhSo]
(
	ChiNhanhId			int,
	MonAnId				int,
	Ngay				datetime,
	TheoNgay			decimal(15,4) DEFAULT 0, -- Số lượng bán được trong ngày.
	TheoTuan			decimal(15,4) DEFAULT 0, -- Số lượng bán được trong tuần.
	TheoThang			decimal(15,4) DEFAULT 0, -- Số lượng bán được trong tháng.
	TheoQuy				decimal(15,4) DEFAULT 0, -- Số lượng bán được trong quý.
	TheoNam				decimal(15,4) DEFAULT 0, -- Số lượng bán được trong năm.
	ThoiGianTao			datetime DEFAULT GETDATE()

	CONSTRAINT PK_ThongKeDoanhSo PRIMARY KEY (ChiNhanhId, MonAnId, Ngay)
);
