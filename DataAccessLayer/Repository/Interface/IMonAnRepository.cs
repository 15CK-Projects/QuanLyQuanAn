﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;

namespace QuanLyQuanAn.DataAccessLayer
{
    public interface IMonAnRepository
    {
        IEnumerable<MonAn> GetAll();
        IEnumerable<MonAn> GetByDanhMucId(int? id);
        MonAn GetMonAn(int id);
        MonAn GetMonAn(MonAn monAn);
        bool InsertMonAn(MonAn monAn);
        bool DeleteMonAn(int id);
        bool DeleteMonAn(MonAn monAn);
        bool UpdateMonAn(MonAn monAn);

        IEnumerable<int> GetAllMonAnIdInKhongThongKe();
        bool IsMonAnIdInKhongThongKe(int monAnId);
        bool InsertMonAnIntoKhongThongKe(int monAnId);
        bool DeleteMonAnIdFromKhongThongKe(int monAnId);
    }
}
