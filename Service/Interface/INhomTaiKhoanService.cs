﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;

namespace QuanLyQuanAn.Service
{
    public interface INhomTaiKhoanService
    {
        IList<NhomTaiKhoan> GetAllNhomTaiKhoan();
        NhomTaiKhoan GetNhomTaiKhoan(int id);
        bool DeleteNhomTaiKhoan(int id);
        bool UpdateNhomTaiKhoan(NhomTaiKhoan nhom);
        bool AddNhomTaiKhoan(NhomTaiKhoan nhom);
    }
}
