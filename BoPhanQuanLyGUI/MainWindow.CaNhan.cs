﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.ComponentModel;
using QuanLyQuanAn.Service;
using QuanLyQuanAn.DataObject;
using QuanLyQuanAn.Common;

namespace QuanLyQuanAn.GUI.BoPhanQuanLyGUI
{
    public partial class MainWindow : Window
    {
        private void DisableMenu()
        {
            Button_DanhMuc.IsEnabled = false;
            Button_MonAn.IsEnabled = false;
            Button_ChiNhanh.IsEnabled = false;
        }

        private void Button_SwitchToLoginTab_Click(object sender, RoutedEventArgs e)
        {
            TabItem_Login.IsSelected = true;
        }

        private void EnableMenu()
        {
            if (WatchDog.HasPermission(Permissions.AccessMonAnDanhMuc))
            {
                Button_DanhMuc.IsEnabled = true;
                Button_MonAn.IsEnabled = true;
            }

            if (WatchDog.HasPermission(Permissions.AccessChiNhanh))
            {
                Button_ChiNhanh.IsEnabled = true;
            }
        }

        private void Button_Login_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TextBox_TenTaiKhoan.Text))
            {
                TextBox_TenTaiKhoan.BorderBrush = Brushes.Red;
                return;
            }

            if (string.IsNullOrEmpty(PasswordBox_MatKhau.Password))
            {
                PasswordBox_MatKhau.BorderBrush = Brushes.Red;
                return;
            }

            bool isOK;
            _Main_TaiKhoanService.Login(TextBox_TenTaiKhoan.Text, PasswordBox_MatKhau.Password, out isOK);
            if (!isOK)
            {
                _Main_DialogManager.CreateMessageDialog()
                    .SetCaption("Thông báo")
                    .SetMessage("Không đăng nhập được")
                    .ShowDialog();
                return;
            }

            EnableMenu();

            Button_SwitchToLoginTab.IsEnabled = false;
            Button_Login.IsEnabled = false;
            Button_Profile.IsEnabled = true;
            Button_Logout.IsEnabled = true;

            ShowProfileInformation();
            TabItem_Profile.IsSelected = true;
        }

        private void Button_Logout_Click(object sender, RoutedEventArgs e)
        {
            DisableMenu();

            Button_SwitchToLoginTab.IsEnabled = true;
            Button_Login.IsEnabled = true;
            Button_Profile.IsEnabled = false;
            Button_Logout.IsEnabled = false;

            TextBox_TenTaiKhoan.Clear();
            PasswordBox_MatKhau.Clear();
            TextBox_MatKhau.Clear();
            
            TabItem_Login.IsSelected = true;
        }

        private void TextBox_TenTaiKhoan_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox_TenTaiKhoan.ClearValue(BorderBrushProperty);
        }

        private void PasswordBox_MatKhau_GotFocus(object sender, RoutedEventArgs e)
        {
            PasswordBox_MatKhau.ClearValue(BorderBrushProperty);
        }

        private void CheckBox_HienThiMatKhau_Checked(object sender, RoutedEventArgs e)
        {
            TextBox_MatKhau.Text = PasswordBox_MatKhau.Password;

            TextBox_MatKhau.Visibility = Visibility.Visible;
            PasswordBox_MatKhau.Visibility = Visibility.Hidden;
        }

        private void CheckBox_HienThiMatKhau_Unchecked(object sender, RoutedEventArgs e)
        {
            TextBox_MatKhau.Visibility = Visibility.Hidden;
            PasswordBox_MatKhau.Visibility = Visibility.Visible;
        }

        private void Button_Profile_Click(object sender, RoutedEventArgs e)
        {
            TabItem_Profile.IsSelected = true;
        }

        private void ShowProfileInformation()
        {
            TaiKhoan taiKhoan = WatchDog.CurrentUser;
            TextBox_Profile_UserName.Text = taiKhoan.UserName;

            if (taiKhoan.NhanVienId == null)
                return;
            NhanVien nhanVien = _Main_NhanVienService.GetNhanVien(taiKhoan.NhanVienId.Value);

            if (nhanVien == null)
                return;
            TextBox_Profile_HoTen.Text = nhanVien.HoTen;
            TextBox_Profile_SoCmnd.Text = nhanVien.SoCmnd;
            TextBox_Profile_SoDienThoai.Text = nhanVien.SoDienThoai;
            TextBox_Profile_DiaChi.Text = nhanVien.DiaChi;
        }
    }
}
