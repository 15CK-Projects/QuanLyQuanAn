﻿CREATE TABLE [dbo].[BanChiNhanh]
(
	Id					int IDENTITY(1,1),
	TenBan				nvarchar(255) NOT NULL,
	ChiNhanhId			int NOT NULL,
	MoTa				ntext,
	ThoiGianTao			datetime DEFAULT GETDATE(),
	ThoiGianCapNhat		datetime DEFAULT GETDATE(),

	CONSTRAINT PK_BanChiNhanh PRIMARY KEY (Id)
);
