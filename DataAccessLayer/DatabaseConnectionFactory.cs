﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;

namespace QuanLyQuanAn.DataAccessLayer
{
    public static class DatabaseConnectionFactory
    {
        private static Func<IDbConnection> _factoryMethod;
        internal static Func<IDbConnection> ClearAssigment = () => null;

        static DatabaseConnectionFactory()
        {
            _factoryMethod = FactoryMethod.DatabaseConnectionFactoryMethod;
        }

        public static IDbConnection Create()
        {
            if (_factoryMethod == null)
            {
                throw new InvalidOperationException("You must call SetFactoryMethod() first.");
            }

            return _factoryMethod();
        }

        public static void SetFactoryMethod(Func<IDbConnection> factoryMethod)
        {
            if (factoryMethod == null)
            {
                throw new ArgumentNullException("factoryMethod");
            }

            _factoryMethod = ClearAssigment == factoryMethod ? null : factoryMethod;
        }
    }
}
