﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanAn.DataAccessLayer
{
    public interface IUnitOfWork : IDisposable
    {
        IDanhMucRepository DanhMucRepository { get; }
        IMonAnRepository MonAnRepository { get; }

        IChiNhanhRepository ChiNhanhRepository { get; }

        IDonHangRepository DonHangRepository { get; }
        IHoaDonRepository HoaDonRepository { get; }

        IKhachHangRepository KhachHangRepository { get; }
        INhanVienRepository NhanVienRepository { get; }

        ITaiKhoanRepository TaiKhoanRepository { get; }
        INhomTaiKhoanRepository NhomTaiKhoanRepository { get; }

        IThongKeRepository ThongKeRepository { get; }

        void Commit();
    }
}
