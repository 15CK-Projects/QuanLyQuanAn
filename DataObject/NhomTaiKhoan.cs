﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanAn.DataObject
{
    public class NhomTaiKhoan
    {
        private int? _id;
        private string _tenNhom;
        private string _quyenNhom;
        private int? _nhomChaId;
        private DateTime? _thoiGianTao;
        private DateTime? _thoiGianCapNhat;

        public int? Id { get => _id; set => _id = value; }
        public string TenNhom { get => _tenNhom; set => _tenNhom = value; }
        public string QuyenNhom { get => _quyenNhom; set => _quyenNhom = value; }
        public int? NhomChaId { get => _nhomChaId; set => _nhomChaId = value; }
        public DateTime? ThoiGianTao { get => _thoiGianTao; set => _thoiGianTao = value; }
        public DateTime? ThoiGianCapNhat { get => _thoiGianCapNhat; set => _thoiGianCapNhat = value; }
    }
}
