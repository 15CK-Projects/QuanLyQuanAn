﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;
using QuanLyQuanAn.DataAccessLayer;

namespace QuanLyQuanAn.Service
{
    public class NhanVienService : INhanVienService
    {
        public NhanVien GetNhanVien(int id)
        {
            using (IUnitOfWork uow = UnitOfWorkFactory.Create())
            {
                var nhanVien = uow.NhanVienRepository.GetNhanVien(id);
                uow.Commit();
                return nhanVien;
            }
        }
    }
}
