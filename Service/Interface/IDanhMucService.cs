﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;

namespace QuanLyQuanAn.Service
{
    public interface IDanhMucService
    {
        IEnumerable<DanhMuc> GetAllDanhMuc();
        bool InsertDanhMuc(DanhMuc danhMuc);
        bool DeleteDanhMuc(int id);
        bool UpdateDanhMuc(DanhMuc danhMuc);
    }
}
