﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;

namespace QuanLyQuanAn.DataAccessLayer
{
    public interface IChiNhanhRepository
    {
        IEnumerable<ChiNhanh> GetAllChiNhanh();
        ChiNhanh GetChiNhanh(int id);
        bool InsertChiNhanh(ChiNhanh chiNhanh);
        bool DeleteChiNhanh(int id);
        bool UpdateChiNhanh(ChiNhanh chiNhanh);

        IEnumerable<BanChiNhanh> FindBanByChiNhanhId(int chiNhanhId);
        BanChiNhanh GetBanChiNhanh(int id);
        bool InsertBanChiNhanh(BanChiNhanh banChiNhanh);
        bool DeleteBanChiNhanh(int id);
        bool UpdateBanChiNhanh(BanChiNhanh banChiNhanh);

        IEnumerable<MonAnChiNhanh> FindMonAnChiNhanhByChiNhanhId(int chiNhanhId);
        MonAnChiNhanh GetMonAnChiNhanh(MonAnChiNhanh menuItem);
        bool InsertMonAnChiNhanh(MonAnChiNhanh monAnChiNhanh);
        bool DeleteMonAnChiNhanh(MonAnChiNhanh monAnChiNhanh);
        bool UpdateMonAnChiNhanh(MonAnChiNhanh monAnChiNhanh);
    }
}
