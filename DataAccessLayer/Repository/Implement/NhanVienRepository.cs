﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;
using Dapper;

namespace QuanLyQuanAn.DataAccessLayer
{
    internal class NhanVienRepository : RepositoryBase, INhanVienRepository
    {
        public NhanVienRepository(IDbTransaction transaction) : base(transaction)
        {

        }

        public bool DeleteNhanVien(int id)
        {
            string sql = @"DELETE FROM [NhanVien] WHERE Id = @Id;";
            int rowsCount = Connection.Execute(sql: sql, param: new { Id = id }, transaction: Transaction);
            if (rowsCount == 1)
                return true;
            return false;
        }

        public NhanVien GetNhanVien(int id)
        {
            string sql = @"SELECT * FROM [NhanVien] WHERE Id = @Id;";
            return Connection.QuerySingleOrDefault<NhanVien>(sql: sql, param: new { Id = id }, transaction: Transaction);
        }

        public bool InsertNhanVien(NhanVien nhanVien)
        {
            string sql = @"INSERT INTO [NhanVien] (HoTen, SoCmnd, SoDienThoai, DiaChi) VALUES (@HoTen, @SoCmnd, @SoDienThoai, @DiaChi); SELECT SCOPE_IDENTITY();";
            try
            {
                nhanVien.Id = Connection.ExecuteScalar<int>(sql: sql, param: nhanVien, transaction: Transaction);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateNhanVien(NhanVien nhanVien)
        {
            nhanVien.ThoiGianCapNhat = DateTime.Now;
            string sql = @"UPDATE [NhanVien] SET HoTen = @HoTen, SoCmnd = @SoCmnd, SoDienThoai = @SoDienThoai, DiaChi = @DiaChi, ThoiGianCapNhat = @ThoiGianCapNhat WHERE Id = @Id;";
            int rowsCount = Connection.Execute(sql: sql, param: nhanVien, transaction: Transaction);
            if (rowsCount == 1)
                return true;
            return false;
        }
    }
}
