﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BanHangTaiChiNhanh
{
    public partial class BanHangTaiChiNhanh : Form
    {
        SqlConnection connection = null;
        SqlCommand command = null;
        string connectionString;
        DataTable dtDanhMuc;
        DataTable dtBan;
        DataTable dtChiNhanh, dtMonAnChiNhanh;
        DataTable dtMonAnTheoDM;
        DataTable dtBill;
        DataTable tb = new DataTable();

        public BanHangTaiChiNhanh()
        {
            InitializeComponent();
            connectionString = @"Data Source=USERITS-QC4NH1M\SQLEXPRESS;Initial Catalog=QuanLyQuanAnDatabase;Integrated Security=True";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            flp_MonAn.Controls.Clear();
            Load_Cbb_ChiNhanh();
            LoadDanhMuc();
            LoadBan();
            LoadMonAnChiNhanh();
            dtBill = new DataTable();
            dtBill.Columns.Add("Mã món ăn");
            dtBill.Columns.Add("Tên món");
            dtBill.Columns.Add("Số lượng");
            dtBill.Columns.Add("Giá");
            dtBill.Columns.Add("Thành tiền");



        }

        private void LoadDanhMuc()
        {
            string sql = "select Id, TenDanhMuc from DanhMuc";
            connection = new SqlConnection(connectionString);
            command = new SqlCommand(sql, connection);

            SqlDataAdapter da = new SqlDataAdapter(command);
            dtDanhMuc = new DataTable();
            da.Fill(dtDanhMuc);


            for(int i=0; i< dtDanhMuc.Rows.Count; i++)
            {
                Button bt = new Button();
                bt.Location = new Point(0, 0);
                bt.Size = new Size(180, 50);
                bt.Text = dtDanhMuc.Rows[i][1].ToString();
                bt.Tag = dtDanhMuc.Rows[i][0].ToString();
                bt.ForeColor = Color.Blue;
                //bt.FlatStyle = FlatStyle.Flat;
                //bt.FlatAppearance.BorderSize = 0;
                bt.Font = new Font("Microsoft Sans Serif",14,FontStyle.Regular);

                bt.Click += DanhMuc_click;
                
                flp_DanhMuc.Controls.Add(bt);

            }

        }
        private void btn_VoBan_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string idBan = btn.Tag.ToString();
            provider p = new provider();
            string sql = string.Format("update BanChiNhanh set MoTa= '1' where Id= {0} and ChiNhanhId= {1}", idBan, cbb_ChonChiNhanh.SelectedValue.ToString());
            p.ExcuteNonQuery(sql, CommandType.Text);

            LoadBan();

            ThemDonHang(idBan);
            ThemChiTietDonHang(idBan);

        }
        private void ThemDonHang(string idBan)
        {
            string sql1 = string.Format("insert into DonHang values(null, '', '', {0}, {1}, {2}, '1'", cbb_ChonChiNhanh.SelectedValue.ToString(), idBan, lb_TongTien.Text);
            provider p = new provider();
            p.ExcuteNonQuery(sql1, CommandType.Text);
        }
        private void ThemChiTietDonHang(string idBan)
        {
            string sql1 = string.Format("insert into ChiTietDonHang values(1, '', '', {0}, {1}, {2}, '1'", cbb_ChonChiNhanh.SelectedValue.ToString(), idBan);
            provider p = new provider();
            p.ExcuteNonQuery(sql1, CommandType.Text);
        }

        private void DanhMuc_click(object sender, EventArgs e)
        {
            
            flp_MonAn.Controls.Clear();
            Button btn = (Button)sender;
            string idDanhMuc = btn.Tag.ToString();
            string sql = string.Format("select Id, TenMonAn, GiaBan, HinhAnh from MonAnChiNhanh , MonAn  where Id= MonAnId and ChiNhanhId= {0} and DanhMucId = {1}", cbb_ChonChiNhanh.SelectedValue, idDanhMuc);
            SqlDataAdapter da = new SqlDataAdapter(sql, connection);
            dtMonAnTheoDM = new DataTable();
            da.Fill(dtMonAnTheoDM);

            for (int i = 0; i < dtMonAnTheoDM.Rows.Count; i++)
            {
                Button bt = new Button();
                bt.Location = new Point(0, 0);
                
                bt.Text = string.Format("{0}, {1}", dtMonAnTheoDM.Rows[i][1].ToString(), dtMonAnTheoDM.Rows[i][2].ToString());
                bt.Tag = dtMonAnTheoDM.Rows[i][0].ToString();
                bt.ForeColor = Color.Black;
                bt.Font = new Font("Microsoft Sans Serif", 10, FontStyle.Regular);
                string image = @"..\..\images\" + dtMonAnTheoDM.Rows[i][3].ToString();
                if (dtMonAnTheoDM.Rows[i][3].ToString() == "")
                    image = @"..\..\images\NoImage.jpg";
                bt.BackgroundImage = Image.FromFile(image);
                bt.BackgroundImageLayout = ImageLayout.Zoom;
                bt.Size = new Size(150, 150);
                //bt.ImageAlign = ContentAlignment.TopCenter;

                bt.TextAlign = ContentAlignment.BottomCenter;
                bt.Click += MonAn_Click;
                
                flp_MonAn.Controls.Add(bt);
                
            }
        }
        private void MonAn_Click(object sender, EventArgs e)
        {
            Button btn1 = (Button)sender;
            string idMonAn = btn1.Tag.ToString();//Lay ra id MonAn
            dgv_DonHang.DataSource = dtBill;
            provider p = new provider();

            string sql = "select * from MonAn as ma where ma.Id = " + idMonAn;
            DataTable dtFood = p.ExcuteQueryTable(sql);

            //tb = p.ExcuteQueryTable(sql, new object[] { idMonAn, cbb_ChonChiNhanh.SelectedValue });

            //tb.Rows.Add(rd[0].ToString(), rd[1].ToString(), rd[2].ToString());
            DataRow Rows = dtBill.NewRow();
            Rows["Mã món ăn"] = dtFood.Rows[0][0].ToString();
            Rows["Tên món"] = dtFood.Rows[0][1].ToString();
            Rows["Số lượng"] = "1";
            Rows["Giá"] = dtFood.Rows[0][5].ToString();
            Rows["Thành tiền"] = float.Parse(Rows["Giá"].ToString()) * float.Parse(Rows["Số lượng"].ToString());
            dtBill.Rows.Add(Rows);
            LoadBill();

        }

        private void LoadBan()
        {
            string sql = string.Format("select Id, TenBan, MoTa from BanChiNhanh where ChiNhanhId= {0}", cbb_ChonChiNhanh.SelectedValue.ToString());

            connection = new SqlConnection(connectionString);
            command = new SqlCommand(sql, connection);

            SqlDataAdapter da = new SqlDataAdapter(command);
          
            dtBan = new DataTable();
            da.Fill(dtBan);
            flp_Ban.Controls.Clear();
            for (int i = 0; i < dtBan.Rows.Count; i++)
            {
                Button bt = new Button();
                bt.Location = new Point(0, 0);
                bt.Size = new Size(140, 50);
                bt.Text = dtBan.Rows[i][1].ToString();
                bt.Tag = dtBan.Rows[i][0].ToString();
                bt.ForeColor = Color.Blue;
                bt.FlatStyle = FlatStyle.Flat;
                bt.FlatAppearance.BorderSize = 0;
                bt.Font = new Font("Microsoft Sans Serif", 12, FontStyle.Regular);
                
                switch (dtBan.Rows[i][2].ToString())
                {
                    case "1":
                        bt.BackColor = Color.OrangeRed;
                        break;
                    default:
                        bt.BackColor = Color.Honeydew;
                        break;
                }
                bt.Click += btn_VoBan_Click;
                flp_Ban.Controls.Add(bt);

            }

            

           
        }

        private void Load_BanHang()
        {

            string sql = "insert into DonHang values() ";
        }

        private void LoadMonAnChiNhanh()
        {
            String idChiNhanh = cbb_ChonChiNhanh.SelectedValue.ToString();
            string sql = "select Id, TenMonAn, GiaBan, HinhAnh from MonAnChiNhanh , MonAn  where Id= MonAnId";
            if (!idChiNhanh.Equals("100"))
            {
                sql += string.Format(" and ChiNhanhId = {0}", idChiNhanh);
            }
            
            connection = new SqlConnection(connectionString);
            SqlDataAdapter da = new SqlDataAdapter(sql, connection);
            dtMonAnChiNhanh = new DataTable();
            da.Fill(dtMonAnChiNhanh);
            flp_MonAn.Controls.Clear();
            for (int i = 0; i < dtMonAnChiNhanh.Rows.Count; i++)
            {
                Button bt = new Button();
                bt.Location = new Point(0, 0);
                bt.Size = new Size(150, 100);
                bt.Text = string.Format("{0}: {1}", dtMonAnChiNhanh.Rows[i][1].ToString(), dtMonAnChiNhanh.Rows[i][2].ToString()); 
                bt.Tag = dtMonAnChiNhanh.Rows[i][0].ToString();
                bt.ForeColor = Color.Black;
                bt.FlatStyle = FlatStyle.Flat;
                bt.FlatAppearance.BorderSize = 0;
                bt.Font = new Font("Microsoft Sans Serif", 10, FontStyle.Bold);
                string image = @"..\..\images\" + dtMonAnChiNhanh.Rows[i][3].ToString();
                if (dtMonAnChiNhanh.Rows[i][3].ToString() == "")
                    image = @"..\..\images\NoImage.jpg";
                bt.Image = Image.FromFile(image);
                bt.ImageAlign = ContentAlignment.TopCenter;
                
                bt.TextAlign = ContentAlignment.BottomCenter;
                bt.Tag = dtMonAnChiNhanh.Rows[i][0].ToString();
                bt.Click += bt_Click;
                flp_MonAn.Controls.Add(bt);

            }

        }

        public void bt_Click(object sender, EventArgs e)
        {
            Button btn1 = (Button)sender;
            string idMonAn = btn1.Tag.ToString();//Lay ra id MonAn
            dgv_DonHang.DataSource = dtBill;
            provider p = new provider();

            string sql = "select * from MonAn as ma where ma.Id = " + idMonAn;
            DataTable dtFood = p.ExcuteQueryTable(sql);

            //tb = p.ExcuteQueryTable(sql, new object[] { idMonAn, cbb_ChonChiNhanh.SelectedValue });

            //tb.Rows.Add(rd[0].ToString(), rd[1].ToString(), rd[2].ToString());
            DataRow Rows = dtBill.NewRow();
            Rows["Mã món ăn"] = dtFood.Rows[0][0].ToString();
            Rows["Tên món"] = dtFood.Rows[0][1].ToString();
            Rows["Số lượng"] = "1";
            Rows["Giá"] = dtFood.Rows[0][5].ToString();
            Rows["Thành tiền"] = float.Parse(Rows["Giá"].ToString()) * float.Parse(Rows["Số lượng"].ToString());
            dtBill.Rows.Add(Rows);



            LoadBill();


            //connection = new SqlConnection(connectionString);
            //command = new SqlCommand(sql, connection);

        }
        private void LoadBill()
        {
            int tien = 0;
            for (int i = 0; i < dtBill.Rows.Count; i++)
            {
                for (int j = i + 1; j < dtBill.Rows.Count; j++)
                {
                    if (dtBill.Rows[i][0].ToString() == dtBill.Rows[j][0].ToString())
                    {
                        dtBill.Rows[i][2] = int.Parse(dtBill.Rows[i][2].ToString()) + 1;
                        dtBill.Rows[i][4] = float.Parse(dtBill.Rows[i][2].ToString()) * float.Parse(dtBill.Rows[i][3].ToString());
                        dtBill.Rows.RemoveAt(j);

                    }
                }
                tien += int.Parse(dtBill.Rows[i][4].ToString());
            }

            dgv_DonHang.DataSource = dtBill;
            lb_TongTien.Text = tien.ToString();
        }


        private void cbb_ChonChiNhanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadBan();
        }

        private void btnHuy_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

       


        private void Load_Cbb_ChiNhanh()
        {


            dtChiNhanh = new DataTable();
            string sql = "select Id, TenChiNhanh from ChiNhanh";
            connection = new SqlConnection(connectionString);
            command = new SqlCommand(sql, connection);
            SqlDataAdapter da = new SqlDataAdapter(command);
            

            da.Fill(dtChiNhanh);
            DataRow tc = dtChiNhanh.NewRow();
            tc["Id"] = "100";
            tc["TenChiNhanh"] = "tất cả";
            dtChiNhanh.Rows.InsertAt(tc, 0);
            cbb_ChonChiNhanh.DataSource = dtChiNhanh;
            cbb_ChonChiNhanh.DisplayMember = "TenChiNhanh";
            cbb_ChonChiNhanh.ValueMember = "Id";

            



        }
        
    }
}

