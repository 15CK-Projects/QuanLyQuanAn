﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;

namespace QuanLyQuanAn.Service
{
    public interface IChiNhanhService
    {
        IEnumerable<ChiNhanh> GetAllChiNhanh();
        ChiNhanh GetChiNhanh(int id);
        bool AddChiNhanh(ChiNhanh chiNhanh);
        bool DeleteChiNhanh(int id);
        bool UpdateChiNhanh(ChiNhanh chiNhanh);

        IEnumerable<BanChiNhanh> FindBanByChiNhanhId(int chiNhanhId);
        bool DuplicateBanChiNhanh(int chiNhanhIdFrom, int chiNhanhIdTo);
        bool AddBanChiNhanh(BanChiNhanh banChiNhanh);
        bool DeleteBanChiNhanh(int id);
        bool DeleteAllBanChiNhanh(int chiNhanhId);
        bool UpdateBanChiNhanh(BanChiNhanh banChiNhanh);

        IEnumerable<MonAnChiNhanh> FindMonAnChiNhanhByChiNhanhId(int chiNhanhId);
        bool DuplicateMonAnChiNhanh(int chiNhanhIdFrom, int chiNhanhIdTo);
        bool AddMonAnChiNhanh(MonAnChiNhanh monAnChiNhanh);
        bool DeleteMonAnChiNhanh(MonAnChiNhanh monAnChiNhanh);
        bool DeleteAllMonAnChiNhanh(int chiNhanhId);
        bool UpdateMonAnChiNhanh(MonAnChiNhanh monAnChiNhanh);
    }
}
