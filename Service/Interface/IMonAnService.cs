﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;

namespace QuanLyQuanAn.Service
{
    public interface IMonAnService
    {
        MonAn GetMonAn(int id);
        IEnumerable<MonAn> GetAllMonAn();
        IEnumerable<MonAn> GetMonAnInDanhMucChuaPhanLoai();
        IEnumerable<MonAn> GetMonAnByDanhMucId(int danhMucId);
        IEnumerable<MonAn> GetTop10MonAnBanChayTatCaChiNhanhTrongThang();
        IEnumerable<MonAn> GetMonAnByDanhMucIdIncludedDefinedDanhMuc(int? danhMucId);
        bool InsertMonAn(MonAn monAn);
        bool DeleteMonAn(int id);
        bool UpdateMonAn(MonAn monAn);

        IEnumerable<int> GetAllMonAnIdInKhongThongKe();
        bool IsMonAnIdInKhongThongKe(int monAnId);
        bool InsertMonAnIntoKhongThongKe(int monAnId);
        bool DeleteMonAnIdFromKhongThongKe(int monAnId);
    }
}
