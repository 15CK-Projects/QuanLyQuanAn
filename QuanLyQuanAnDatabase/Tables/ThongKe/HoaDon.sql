﻿CREATE TABLE [dbo].[HoaDon]
(
	Id						int,
	ThoiGianTao				datetime DEFAULT GETDATE(),
	DonHangId				int,
	DaThanhToan				bit DEFAULT 0,

	CONSTRAINT PK_HoaDon PRIMARY KEY (Id)
);
