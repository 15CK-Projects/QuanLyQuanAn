﻿CREATE PROCEDURE [dbo].[USP_Top10MonAnBanChayTatCaChiNhanh]
	@from						date,
	@to							date
AS
	WITH Top10MonAnId_CTE (MonAnId, SoLuong)
	AS
	(
		SELECT TOP (10) TK.MonAnId, SUM(TK.TheoNgay) AS SoLuong
		FROM [ThongKeDoanhSo] TK
		WHERE TK.Ngay >= @from AND TK.Ngay <= @to
			AND TK.MonAnId NOT IN
				(
					-- Không liệt kê đối với những món ăn trong danh sách "không thống kê bán chạy".
					SELECT KTK.MonAnId
					FROM [dbo].[KhongThongKe] KTK
				)
		GROUP BY TK.MonAnId
		ORDER BY SUM(TK.TheoNgay) DESC
	)

	SELECT *
	FROM Top10MonAnId_CTE T10
		INNER JOIN [dbo].[MonAn] MA ON T10.MonAnId = MA.Id
	ORDER BY T10.SoLuong DESC, MA.TenMonAn ASC;
RETURN 0
