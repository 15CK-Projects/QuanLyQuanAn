﻿CREATE TABLE [dbo].[ThongKeDoanhThu]
(
	ChiNhanhId						int,
	Ngay							datetime,
	DoanhThuNgay					decimal(15,4) DEFAULT 0,                 
	DoanhThuTuan					decimal(15,4) DEFAULT 0,
	DoanhThuThang					decimal(15,4) DEFAULT 0,
	DoanhThuQuy						decimal(15,4) DEFAULT 0,
	DoanhThuNam						decimal(15,4) DEFAULT 0,
	ThoiGianTao						datetime DEFAULT GETDATE(),

	CONSTRAINT PK_ThongKeDoanhThu PRIMARY KEY (ChiNhanhId, Ngay)
);
