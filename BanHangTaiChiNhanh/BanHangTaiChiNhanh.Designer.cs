﻿namespace BanHangTaiChiNhanh
{
    partial class BanHangTaiChiNhanh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BanHangTaiChiNhanh));
            this.label1 = new System.Windows.Forms.Label();
            this.cbb_ChonChiNhanh = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pn_DanhMuc = new System.Windows.Forms.Panel();
            this.flp_Ban = new System.Windows.Forms.FlowLayoutPanel();
            this.tb_ThHienBan = new System.Windows.Forms.TextBox();
            this.pn_MonAn = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.flp_DanhMuc = new System.Windows.Forms.FlowLayoutPanel();
            this.flp_MonAn = new System.Windows.Forms.FlowLayoutPanel();
            this.tb_DanhMuc = new System.Windows.Forms.TextBox();
            this.tb_TimMonAn = new System.Windows.Forms.TextBox();
            this.pn_BanHang = new System.Windows.Forms.Panel();
            this.lb_TongTien = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dgv_DonHang = new System.Windows.Forms.DataGridView();
            this.btn_Huy = new System.Windows.Forms.Button();
            this.btnHuy = new System.Windows.Forms.Button();
            this.btn_ThanhToan = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_VoBan = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.pn_DanhMuc.SuspendLayout();
            this.pn_MonAn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.pn_BanHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_DonHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(911, 19);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(206, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "CHỌN CHI NHÁNH:";
            // 
            // cbb_ChonChiNhanh
            // 
            this.cbb_ChonChiNhanh.DisplayMember = "TenChiNhanh";
            this.cbb_ChonChiNhanh.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_ChonChiNhanh.FormattingEnabled = true;
            this.cbb_ChonChiNhanh.Location = new System.Drawing.Point(1124, 12);
            this.cbb_ChonChiNhanh.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cbb_ChonChiNhanh.Name = "cbb_ChonChiNhanh";
            this.cbb_ChonChiNhanh.Size = new System.Drawing.Size(166, 33);
            this.cbb_ChonChiNhanh.TabIndex = 2;
            this.cbb_ChonChiNhanh.ValueMember = "Id";
            this.cbb_ChonChiNhanh.SelectedIndexChanged += new System.EventHandler(this.cbb_ChonChiNhanh_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(168)))));
            this.panel1.Controls.Add(this.cbb_ChonChiNhanh);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1516, 68);
            this.panel1.TabIndex = 4;
            // 
            // pn_DanhMuc
            // 
            this.pn_DanhMuc.Controls.Add(this.flp_Ban);
            this.pn_DanhMuc.Controls.Add(this.tb_ThHienBan);
            this.pn_DanhMuc.Location = new System.Drawing.Point(0, 73);
            this.pn_DanhMuc.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pn_DanhMuc.Name = "pn_DanhMuc";
            this.pn_DanhMuc.Size = new System.Drawing.Size(195, 688);
            this.pn_DanhMuc.TabIndex = 5;
            // 
            // flp_Ban
            // 
            this.flp_Ban.AutoScroll = true;
            this.flp_Ban.Location = new System.Drawing.Point(7, 58);
            this.flp_Ban.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.flp_Ban.Name = "flp_Ban";
            this.flp_Ban.Size = new System.Drawing.Size(183, 613);
            this.flp_Ban.TabIndex = 1;
            // 
            // tb_ThHienBan
            // 
            this.tb_ThHienBan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_ThHienBan.Location = new System.Drawing.Point(2, 10);
            this.tb_ThHienBan.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tb_ThHienBan.Name = "tb_ThHienBan";
            this.tb_ThHienBan.Size = new System.Drawing.Size(187, 28);
            this.tb_ThHienBan.TabIndex = 0;
            // 
            // pn_MonAn
            // 
            this.pn_MonAn.Controls.Add(this.pictureBox2);
            this.pn_MonAn.Controls.Add(this.flp_DanhMuc);
            this.pn_MonAn.Controls.Add(this.flp_MonAn);
            this.pn_MonAn.Controls.Add(this.tb_DanhMuc);
            this.pn_MonAn.Controls.Add(this.tb_TimMonAn);
            this.pn_MonAn.Location = new System.Drawing.Point(217, 73);
            this.pn_MonAn.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pn_MonAn.Name = "pn_MonAn";
            this.pn_MonAn.Size = new System.Drawing.Size(698, 688);
            this.pn_MonAn.TabIndex = 5;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(206, 47);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(17, 631);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // flp_DanhMuc
            // 
            this.flp_DanhMuc.AutoScroll = true;
            this.flp_DanhMuc.Location = new System.Drawing.Point(12, 58);
            this.flp_DanhMuc.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.flp_DanhMuc.Name = "flp_DanhMuc";
            this.flp_DanhMuc.Size = new System.Drawing.Size(189, 609);
            this.flp_DanhMuc.TabIndex = 4;
            // 
            // flp_MonAn
            // 
            this.flp_MonAn.AutoScroll = true;
            this.flp_MonAn.Location = new System.Drawing.Point(236, 63);
            this.flp_MonAn.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.flp_MonAn.Name = "flp_MonAn";
            this.flp_MonAn.Size = new System.Drawing.Size(440, 608);
            this.flp_MonAn.TabIndex = 3;
            // 
            // tb_DanhMuc
            // 
            this.tb_DanhMuc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_DanhMuc.Location = new System.Drawing.Point(12, 10);
            this.tb_DanhMuc.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tb_DanhMuc.Name = "tb_DanhMuc";
            this.tb_DanhMuc.Size = new System.Drawing.Size(186, 28);
            this.tb_DanhMuc.TabIndex = 0;
            // 
            // tb_TimMonAn
            // 
            this.tb_TimMonAn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_TimMonAn.Location = new System.Drawing.Point(236, 10);
            this.tb_TimMonAn.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tb_TimMonAn.Name = "tb_TimMonAn";
            this.tb_TimMonAn.Size = new System.Drawing.Size(440, 28);
            this.tb_TimMonAn.TabIndex = 0;
            // 
            // pn_BanHang
            // 
            this.pn_BanHang.Controls.Add(this.lb_TongTien);
            this.pn_BanHang.Controls.Add(this.label2);
            this.pn_BanHang.Controls.Add(this.dgv_DonHang);
            this.pn_BanHang.Controls.Add(this.btn_Huy);
            this.pn_BanHang.Controls.Add(this.btnHuy);
            this.pn_BanHang.Controls.Add(this.btn_VoBan);
            this.pn_BanHang.Controls.Add(this.btn_ThanhToan);
            this.pn_BanHang.Location = new System.Drawing.Point(935, 73);
            this.pn_BanHang.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pn_BanHang.Name = "pn_BanHang";
            this.pn_BanHang.Size = new System.Drawing.Size(562, 684);
            this.pn_BanHang.TabIndex = 6;
            // 
            // lb_TongTien
            // 
            this.lb_TongTien.AutoSize = true;
            this.lb_TongTien.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_TongTien.Location = new System.Drawing.Point(210, 510);
            this.lb_TongTien.Name = "lb_TongTien";
            this.lb_TongTien.Size = new System.Drawing.Size(0, 25);
            this.lb_TongTien.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(120, 510);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tổng tiền:";
            // 
            // dgv_DonHang
            // 
            this.dgv_DonHang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_DonHang.Location = new System.Drawing.Point(24, 37);
            this.dgv_DonHang.Name = "dgv_DonHang";
            this.dgv_DonHang.RowTemplate.Height = 24;
            this.dgv_DonHang.Size = new System.Drawing.Size(519, 422);
            this.dgv_DonHang.TabIndex = 1;
            // 
            // btn_Huy
            // 
            this.btn_Huy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(168)))));
            this.btn_Huy.FlatAppearance.BorderSize = 0;
            this.btn_Huy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Huy.Location = new System.Drawing.Point(327, 598);
            this.btn_Huy.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btn_Huy.Name = "btn_Huy";
            this.btn_Huy.Size = new System.Drawing.Size(125, 50);
            this.btn_Huy.TabIndex = 0;
            this.btn_Huy.Text = "Hủy";
            this.btn_Huy.UseVisualStyleBackColor = false;
            // 
            // btnHuy
            // 
            this.btnHuy.BackColor = System.Drawing.Color.White;
            this.btnHuy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnHuy.FlatAppearance.BorderSize = 0;
            this.btnHuy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHuy.Image = ((System.Drawing.Image)(resources.GetObject("btnHuy.Image")));
            this.btnHuy.Location = new System.Drawing.Point(475, 598);
            this.btnHuy.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(68, 50);
            this.btnHuy.TabIndex = 0;
            this.btnHuy.UseVisualStyleBackColor = false;
            this.btnHuy.Click += new System.EventHandler(this.btnHuy_Click_1);
            // 
            // btn_ThanhToan
            // 
            this.btn_ThanhToan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(168)))));
            this.btn_ThanhToan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_ThanhToan.FlatAppearance.BorderSize = 0;
            this.btn_ThanhToan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ThanhToan.Location = new System.Drawing.Point(189, 598);
            this.btn_ThanhToan.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btn_ThanhToan.Name = "btn_ThanhToan";
            this.btn_ThanhToan.Size = new System.Drawing.Size(125, 50);
            this.btn_ThanhToan.TabIndex = 0;
            this.btn_ThanhToan.Text = "Thanh toán";
            this.btn_ThanhToan.UseVisualStyleBackColor = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(195, 120);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(29, 631);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btn_VoBan
            // 
            this.btn_VoBan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(168)))));
            this.btn_VoBan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_VoBan.FlatAppearance.BorderSize = 0;
            this.btn_VoBan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_VoBan.Location = new System.Drawing.Point(33, 598);
            this.btn_VoBan.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btn_VoBan.Name = "btn_VoBan";
            this.btn_VoBan.Size = new System.Drawing.Size(125, 50);
            this.btn_VoBan.TabIndex = 0;
            this.btn_VoBan.Text = "Vào bàn";
            this.btn_VoBan.UseVisualStyleBackColor = false;
            this.btn_VoBan.Click += new System.EventHandler(this.btn_VoBan_Click);
            // 
            // BanHangTaiChiNhanh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1516, 774);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pn_BanHang);
            this.Controls.Add(this.pn_MonAn);
            this.Controls.Add(this.pn_DanhMuc);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "BanHangTaiChiNhanh";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BanHangTaiChiNhanh";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pn_DanhMuc.ResumeLayout(false);
            this.pn_DanhMuc.PerformLayout();
            this.pn_MonAn.ResumeLayout(false);
            this.pn_MonAn.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.pn_BanHang.ResumeLayout(false);
            this.pn_BanHang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_DonHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbb_ChonChiNhanh;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pn_DanhMuc;
        private System.Windows.Forms.Panel pn_MonAn;
        private System.Windows.Forms.TextBox tb_TimMonAn;
        private System.Windows.Forms.Panel pn_BanHang;
        private System.Windows.Forms.Button btn_Huy;
        private System.Windows.Forms.Button btn_ThanhToan;
        private System.Windows.Forms.FlowLayoutPanel flp_Ban;
        private System.Windows.Forms.TextBox tb_ThHienBan;
        private System.Windows.Forms.FlowLayoutPanel flp_MonAn;
        private System.Windows.Forms.TextBox tb_DanhMuc;
        private System.Windows.Forms.FlowLayoutPanel flp_DanhMuc;
        private System.Windows.Forms.DataGridView dgv_DonHang;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnHuy;
        private System.Windows.Forms.Label lb_TongTien;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_VoBan;
    }
}

