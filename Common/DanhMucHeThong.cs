﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;

namespace QuanLyQuanAn.Common
{
    public static class DanhMucHeThong
    {
        public static readonly DanhMuc ChuaPhanLoai;
        public static readonly DanhMuc TatCa;
        public static readonly DanhMuc Top10;

        static DanhMucHeThong()
        {
            ChuaPhanLoai = new DanhMuc { Id = -1, TenDanhMuc = "Chưa phân loại", MoTa = "Chứa các món ăn không thuộc bất kì danh mục nào." };
            TatCa = new DanhMuc { Id = -2, TenDanhMuc = "Tất cả", MoTa = "Chứa các món ăn của tất cả các danh mục." };
            Top10 = new DanhMuc { Id = -3, TenDanhMuc = "Top 10", MoTa = "Chứa 10 món ăn bán chạy nhất (trên tất cả các chi nhánh)." };
        }
    }
}
