﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;
using Dapper;

namespace QuanLyQuanAn.DataAccessLayer
{
    internal class NhomTaiKhoanRepository : RepositoryBase, INhomTaiKhoanRepository
    {
        public NhomTaiKhoanRepository(IDbTransaction transaction) : base(transaction)
        {

        }

        public bool AddNhomTaiKhoan(NhomTaiKhoan nhom)
        {
            string sql = @"INSERT INTO NhomTaiKhoan (TenNhom, QuyenNhom VALUES (@TenNhom, @QuyenNhom, @NhomChaId);";
            int rowsCount = Connection.Execute(sql: sql, param: nhom, transaction: Transaction);
            if (rowsCount == 0)
            {
                return false;
            }
            return true;
        }

        public bool DeleteNhomTaiKhoan(int id)
        {
            string sql = @"DELETE FROM NhomTaiKhoan WHERE Id = @Id;";
            int rowsCount = Connection.Execute(sql: sql, param: new { Id = id }, transaction: Transaction);
            if (rowsCount == 0)
            {
                return false;
            }
            return true;
        }

        public bool DeleteNhomTaiKhoan(NhomTaiKhoan nhom)
        {
            if (nhom == null || !nhom.Id.HasValue)
            {
                throw new ArgumentNullException("nhom");
            }

            return DeleteNhomTaiKhoan(nhom.Id.Value);
        }

        public IEnumerable<NhomTaiKhoan> GetAll()
        {
            string sql = @"SELECT * FROM NhomTaiKhoan;";
            var queryResult = Connection.Query<NhomTaiKhoan>(sql: sql, param: null, transaction: Transaction);
            return queryResult;
        }

        public NhomTaiKhoan GetNhomTaiKhoan(int id)
        {
            string sql = @"SELECT * FROM NhomTaiKhoan WHERE Id = @Id;";
            var queryResult = Connection.QuerySingleOrDefault<NhomTaiKhoan>(sql: sql, param: new { Id = id }, transaction: Transaction);
            return queryResult;
        }

        public NhomTaiKhoan GetNhomTaiKhoan(NhomTaiKhoan nhom)
        {
            if (nhom == null || !nhom.Id.HasValue)
            {
                throw new ArgumentNullException("nhom");
            }

            return GetNhomTaiKhoan(nhom.Id.Value);
        }

        public bool UpdateNhomTaiKhoan(NhomTaiKhoan nhom)
        {
            if (nhom == null || !nhom.Id.HasValue)
            {
                throw new ArgumentNullException("nhom");
            }
                 
            nhom.ThoiGianCapNhat = DateTime.Now;

            string sql = @"UPDATE NhomTaiKhoan SET TenNhom = @TenNhom, QuyenNhom = @QuyenNhom, NhomChaId = @NhomChaId, ThoiGianCapNhat = @ThoiGianCapNhat WHERE Id = @Id";
            int rowsCount = Connection.Execute(sql: sql, param: nhom, transaction: Transaction);
            if (rowsCount == 0)
            {
                return false;
            }
            return true;
        }
    }
}
