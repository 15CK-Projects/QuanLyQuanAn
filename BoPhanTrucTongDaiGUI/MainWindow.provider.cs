﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanAn.GUI.BoPhanQuanLyGUI
{
    public class Provider
    {
        public String ConnectionString = @"Data Source=KHANHHUYEN;Initial Catalog=QuanLyQuanAnDatabase;Integrated Security=True";
        // giu chuoi
        SqlConnection Connection = null;
        //mo ket noi CSDL
        public SqlConnection GetConnect()
        {

            return Connection = new SqlConnection(ConnectionString);
        }

        public void Connect()
        {
            try
            {
                if (Connection == null)
                    Connection = new SqlConnection(ConnectionString);
                if (Connection.State != ConnectionState.Closed)
                    Connection.Close();
                Connection.Open();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
        //ngat ket noi CSDL
        public void Disconnect()
        {
            if (Connection != null && Connection.State == ConnectionState.Open)
                Connection.Close();
        }
        //them,xoa,sua du lieu khong dung tham so
        public int ExecuteNonQuery(CommandType cmdType, string strSql)
        {
            try
            {
                SqlCommand Command = new SqlCommand();
                Command.CommandText = strSql;
                Command.CommandType = cmdType;
                int nRow = Command.ExecuteNonQuery();
                return nRow;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        //them,xoa,sua du lieu co tham so
        public int ExecuteNonQuery(CommandType cmdType, string strSql, params SqlParameter[] parameters)
        {
            try
            {
                SqlCommand command = Connection.CreateCommand();
                command.CommandText = strSql;
                command.CommandType = cmdType;
                if (parameters != null && parameters.Length > 0)
                    command.Parameters.AddRange(parameters);
                int nRow = command.ExecuteNonQuery();
                return nRow;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
        //Tim kiem du lieu
        //Cach1:
        public SqlDataReader GetReader(CommandType cmdType, string strSql)
        {
            try
            {
                SqlCommand command = Connection.CreateCommand();
                command.CommandText = strSql;
                command.CommandType = cmdType;
                return command.ExecuteReader();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
        //Cach2:
        public DataTable Select(CommandType cmdType, string strSql)
        {
            try
            {
                SqlCommand command = new SqlCommand(strSql, new SqlConnection(ConnectionString));
                SqlDataAdapter da = new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
