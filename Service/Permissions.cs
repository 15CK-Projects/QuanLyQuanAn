﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanAn.Service
{
    public static class Permissions
    {
        public static readonly string CanLogin = "QuanLyQuanAn.General.Login";
        public static readonly string CanLogout = "QuanLyQuanAn.General.Logout";

        public static readonly string AccessMonAnDanhMuc = "QuanLyQuanAn.QuanLy.MonAnDanhMuc.Access";
        public static readonly string EditMonAnDanhMuc = "QuanLyQuanAn.QuanLy.MonAnDanhMuc.Edit";

        public static readonly string AccessChiNhanh = "QuanLyQuanAn.QuanLy.ChiNhanh.Access";
        public static readonly string EditChiNhanh = "QuanLyQuanAn.QuanLy.ChiNhanh.Edit";

        public static readonly string AccessBaoCao = "QuanLyQuanAn.QuanLy.BaoCao.Access";

        public static readonly string AccessPhanQuyen = "QuanLyQuanAn.HeThong.PhanQuyen.Access";
        public static readonly string EditPhanQuyen = "QuanLyQuanAn.HeThong.PhanQuyen.Edit";

        public static readonly string AccessTaiKhoan = "QuanLyQuanAn.HeThong.TaiKhoan.Access";
        public static readonly string EditTaiKhoan = "QuanLyQuanAn.HeThong.TaiKhoan.Edit";

        public static List<string> DefaultPermissions { get; private set; }

        static Permissions()
        {
            DefaultPermissions = new List<string>()
            {
                CanLogin,
                CanLogout,
                AccessMonAnDanhMuc,
                AccessChiNhanh
            };
        }
    }
}
