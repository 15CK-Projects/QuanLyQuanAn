﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;
using Dapper;

namespace QuanLyQuanAn.DataAccessLayer
{
    internal class ChiNhanhRepository : RepositoryBase, IChiNhanhRepository
    {
        public ChiNhanhRepository(IDbTransaction transaction) : base(transaction)
        {
            
        }

        public bool DeleteBanChiNhanh(int id)
        {
            string sql = @"DELETE FROM [BanChiNhanh] WHERE Id = @Id;";
            int rowsCount = Connection.Execute(sql: sql, param: new { Id = id }, transaction: Transaction);
            if (rowsCount == 1)
                return true;
            return false;
        }

        public bool DeleteChiNhanh(int id)
        {
            string sql = @"DELETE FROM [ChiNhanh] WHERE Id = @Id;";
            int rowsCount = Connection.Execute(sql: sql, param: new { Id = id }, transaction: Transaction);
            if (rowsCount == 0)
                return false;
            return true;
        }

        public bool DeleteMonAnChiNhanh(MonAnChiNhanh monAnChiNhanh)
        {
            string sql = @"DELETE FROM [MonAnChiNhanh] WHERE MonAnId = @MonAnId AND ChiNhanhId = @ChiNhanhId;";
            int rowsCount = Connection.Execute(sql: sql, param: monAnChiNhanh, transaction: Transaction);
            if (rowsCount == 1)
                return true;
            return false;
        }

        public IEnumerable<BanChiNhanh> FindBanByChiNhanhId(int chiNhanhId)
        {
            string sql = @"SELECT * FROM [BanChiNhanh] WHERE ChiNhanhId = @ChiNhanhId;";
            return Connection.Query<BanChiNhanh>(sql: sql, param: new { ChiNhanhid = chiNhanhId }, transaction: Transaction);
        }

        public IEnumerable<ChiNhanh> GetAllChiNhanh()
        {
            string sql = @"SELECT * FROM [ChiNhanh];";
            return Connection.Query<ChiNhanh>(sql: sql, param: null, transaction: Transaction);
        }

        public IEnumerable<MonAnChiNhanh> FindMonAnChiNhanhByChiNhanhId(int chiNhanhId)
        {
            string sql = @"SELECT * FROM [MonAnChiNhanh] WHERE ChiNhanhId = @ChiNhanhId;";
            return Connection.Query<MonAnChiNhanh>(sql: sql, param: new { ChiNhanhId = chiNhanhId }, transaction: Transaction);
        }

        public BanChiNhanh GetBanChiNhanh(int id)
        {
            string sql = @"SELECT * FROM [BanChiNhanh] WHERE Id = @Id;";
            return Connection.QuerySingleOrDefault<BanChiNhanh>(sql: sql, param: new { Id = id }, transaction: Transaction);
        }

        public ChiNhanh GetChiNhanh(int id)
        {
            string sql = @"SELECT * FROM [ChiNhanh] WHERE Id = @Id;";
            return Connection.QuerySingleOrDefault<ChiNhanh>(sql: sql, param: new { Id = id }, transaction: Transaction);
        }

        public MonAnChiNhanh GetMonAnChiNhanh(MonAnChiNhanh monAnChiNhanh)
        {
            string sql = @"SELECT * FROM [MonAnChiNhanh] WHERE MonAnId = @MonAnId AND ChiNhanhId = @ChiNhanhId;";
            return Connection.QuerySingleOrDefault<MonAnChiNhanh>(sql: sql, param: monAnChiNhanh, transaction: Transaction);
        }

        public bool InsertBanChiNhanh(BanChiNhanh banChiNhanh)
        {
            string sql = @"INSERT INTO [BanChiNhanh] (TenBan, ChiNhanhId, MoTa) VALUES (@TenBan, @ChiNhanhId, @MoTa); SELECT SCOPE_IDENTITY();";
            try
            {
                banChiNhanh.Id = Connection.ExecuteScalar<int>(sql: sql, param: banChiNhanh, transaction: Transaction);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool InsertChiNhanh(ChiNhanh chiNhanh)
        {
            string sql = @"INSERT INTO [ChiNhanh] (TenChiNhanh, SoDienThoai, DiaChi) VALUES (@TenChiNhanh, @SoDienThoai, @DiaChi); SELECT SCOPE_IDENTITY();";
            try
            {
                chiNhanh.Id = Connection.ExecuteScalar<int>(sql: sql, param: chiNhanh, transaction: Transaction);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool InsertMonAnChiNhanh(MonAnChiNhanh menuItem)
        {
            try
            {
                string sql = @"INSERT INTO [MonAnChiNhanh] (MonAnId, ChiNhanhId, GiaBan) VALUES (@MonAnId, @ChiNhanhId, @GiaBan)";
                Connection.Execute(sql: sql, param: menuItem, transaction: Transaction);
                return true;
            }
            catch
            {
                return false;
            }            
        }

        public bool UpdateBanChiNhanh(BanChiNhanh banChiNhanh)
        {
            string sql = @"UPDATE [BanChiNhanh] SET TenBan = @TenBan WHERE Id = @Id;";
            banChiNhanh.ThoiGianCapNhat = DateTime.Now;
            int rowsCount = Connection.Execute(sql: sql, param: banChiNhanh, transaction: Transaction);
            if (rowsCount == 1)
                return true;
            return false;
        }

        public bool UpdateChiNhanh(ChiNhanh chiNhanh)
        {
            string sql = @"UPDATE [ChiNhanh] SET TenChiNhanh = @TenChiNhanh, SoDienThoai = @SoDienThoai, DiaChi = @DiaChi, ThoiGianCapNhat = @ThoiGianCapNhat WHERE Id = @Id;";
            chiNhanh.ThoiGianCapNhat = DateTime.Now;
            int rowsCount = Connection.Execute(sql: sql, param: chiNhanh, transaction: Transaction);
            if (rowsCount == 1)
                return true;
            return false;
        }

        public bool UpdateMonAnChiNhanh(MonAnChiNhanh menuItem)
        {
            string sql = @"UPDATE [MonAnChiNhanh] SET GiaBan = @GiaBan, ThoiGianCapNhat = @ThoiGianCapNhat WHERE MonAnId = @MonAnId AND ChiNhanhId = @ChiNhanhId;";
            menuItem.ThoiGianCapNhat = DateTime.Now;
            int rowsCount = Connection.Execute(sql: sql, param: menuItem, transaction: Transaction);
            if (rowsCount == 1)
                return true;
            return false;
        }
    }
}
