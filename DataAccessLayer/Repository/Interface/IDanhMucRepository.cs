﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyQuanAn.DataObject;

namespace QuanLyQuanAn.DataAccessLayer
{
    public interface IDanhMucRepository
    {
        IEnumerable<DanhMuc> GetAll();
        DanhMuc GetDanhMuc(int id);
        DanhMuc GetDanhMuc(DanhMuc danhMuc);
        bool InsertDanhMuc(DanhMuc danhMuc);
        bool DeleteDanhMuc(int id);
        bool DeleteDanhMuc(DanhMuc danhMuc);
        bool UpdateDanhMuc(DanhMuc danhMuc);
    }
}
