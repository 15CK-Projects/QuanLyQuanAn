﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanAn.DataObject
{
    public class MonAnChiNhanh
    {
        private int? _monAnId;
        private int? _chiNhanhId;
        private decimal? _giaBan;
        private DateTime? _thoiGianTao;
        private DateTime? _thoiGianCapNhat;

        public int? MonAnId { get => _monAnId; set => _monAnId = value; }
        public int? ChiNhanhId { get => _chiNhanhId; set => _chiNhanhId = value; }
        public decimal? GiaBan { get => _giaBan; set => _giaBan = value; }
        public DateTime? ThoiGianTao { get => _thoiGianTao; set => _thoiGianTao = value; }
        public DateTime? ThoiGianCapNhat { get => _thoiGianCapNhat; set => _thoiGianCapNhat = value; }
    }
}
