﻿using QuanLyQuanAn.GUI.BoPhanQuanLyGUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyQuanAn.GUI.BoPhanTrucTongDaiGUI
{
    public partial class ThemKhachHang : Form
    {
        public ThemKhachHang()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLuuKhachHang_Click(object sender, EventArgs e)
        {
            Provider pro = new Provider();
            String sql = "INSERT INTO dbo.KhachHang (Ma, SoDienThoai,GioiTinh, NgaySinh, SoCMND, DiaChiMuc1, DiaChiMuc2, DiaChiMuc3, ThoiGianTao, ThoiGianCapNhat) " +
                "values(@Ma, @SoDienThoai, @GioiTinh, @NgaySinh, @SoCMND, @DiaChiMuc1, @DiaChiMuc2, @DiaChiMuc3, @ThoiGianTao, @ThoiGianCapNhat)";

            var day = "20/10/2017";

            using (SqlConnection connection = new SqlConnection(pro.ConnectionString))
            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                //a shorter syntax to adding parameters
                command.Parameters.Add("@Ma", SqlDbType.Int).Value = 3;

                command.Parameters.Add("@SoDienThoai", SqlDbType.NVarChar).Value = txtSDT;

                command.Parameters.Add("@GioiTinh", SqlDbType.NVarChar).Value = "Name";

                command.Parameters.Add("@NgaySinh", SqlDbType.DateTime).Value = Convert.ToDateTime(day);

                command.Parameters.Add("@SoCMND", SqlDbType.NVarChar).Value = txtSoCMND;

                command.Parameters.Add("@DiaChiMuc1", SqlDbType.NVarChar).Value = "abc";

                command.Parameters.Add("@DiaChiMuc2", SqlDbType.NVarChar).Value = "abc";

                command.Parameters.Add("@DiaChiMuc3", SqlDbType.NVarChar).Value = "abc";

                command.Parameters.Add("@ThoiGianTao", SqlDbType.DateTime).Value = Convert.ToDateTime(day);

                command.Parameters.Add("@ThoiGianCapNhat", SqlDbType.DateTime).Value = Convert.ToDateTime(day);
                //make sure you open and close(after executing) the connection
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
            }




        }
    }
}
