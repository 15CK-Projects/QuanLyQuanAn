﻿CREATE TABLE [dbo].[DanhMuc]
(
	Id					int IDENTITY(1,1),
	TenDanhMuc			nvarchar(255),
	MoTa				ntext,
	HinhAnh				ntext,
	ThoiGianTao			datetime DEFAULT GETDATE(),
	ThoiGianCapNhat		datetime DEFAULT GETDATE(),

	CONSTRAINT PK_DanhMuc PRIMARY KEY ([Id])
);
