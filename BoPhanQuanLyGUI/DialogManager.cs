﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace QuanLyQuanAn.GUI.BoPhanQuanLyGUI
{
    public class DialogManager
    {
        public enum DialogResult
        {
            Abort,
            Cancel,
            Ignore,
            No,
            OK,
            Retry,
            Yes
        }

        public enum DialogButton
        {
           AbortRetryIgnore,
           OK,
           OKCancel,
           RetryCancel,
           YesNo,
           YesNoCancel
        }

        public enum DialogImage
        {
            Asterisk,
            Error,
            Exclamation,
            Hand,
            Information,
            None,
            Question,
            Stop,
            Warning
        }

        public Window Owner { get; set; }

        public DialogManager(Window owner)
        {
            Owner = owner;
        }

        public MessageDialog CreateMessageDialog()
        {
            MessageDialog messageDialog = new MessageDialog();
            messageDialog.MessageDialogImage = DialogImage.None;
            messageDialog.MessageDialogButton = DialogButton.OK;
            messageDialog.MessageDialogDefaultButton = DialogResult.OK;
            messageDialog.MessageDialogResult = messageDialog.MessageDialogDefaultButton;
            messageDialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            messageDialog.Owner = Owner;
            return messageDialog;
        }
    }
}
