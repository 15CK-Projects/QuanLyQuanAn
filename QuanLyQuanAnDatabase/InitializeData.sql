﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

INSERT INTO [dbo].[TrangThaiDonHang] (Id, TenTrangThai)
VALUES
(1, N'Mới tạo'),
(2, N'Đang giao'),
(3, N'Đã giao')
;

INSERT INTO [dbo].[NhanVien] ([HoTen], [SoCmnd], [SoDienThoai], [DiaChi])
VALUES
(N'Trương Châu Hiền', N'0123456789', N'0123456789', N'271/2 An Dương Vương, Quận 5, Tp. HCM')
;

INSERT INTO [dbo].[ThongKeDoanhSo] ([ChiNhanhId], [MonAnId], [Ngay], [TheoNgay])
VALUES
(1, 1, '2017/12/20', 10),
(1, 2, '2017/12/20', 10),
(1, 3, '2017/12/20', 10),
(1, 4, '2017/12/20', 10),
(1, 5, '2017/12/20', 10),
(1, 6, '2017/12/20', 10),
(1, 7, '2017/12/20', 10),
(1, 8, '2017/12/20', 10),
(1, 9, '2017/12/20', 10),
(1, 10, '2017/12/20', 10),
(1, 11, '2017/12/20', 5)
;

INSERT INTO [dbo].[TaiKhoan] ([UserName], [Password], [NhomTaiKhoanId], [NhanVienId])
VALUES
(N'truongchauhien', N'abcABCabc@@@*#', 2, 1),
(N'sa', N'abcABCabc@@*#', 3, 1)
;

INSERT INTO [dbo].[NhomTaiKhoan] ([TenNhom], [QuyenNhom], [NhomChaId])
VALUES
(N'Default', N'QuanLyQuanAn.General.Login;QuanLyQuanAn.General.Logout', NULL),
(N'QuanLy', N'QuanLyQuanAn.QuanLy.MonAnDanhMuc.Access;QuanLyQuanAn.QuanLy.MonAnDanhMuc.Edit;QuanLyQuanAn.QuanLy.ChiNhanh.Access;QuanLyQuanAn.QuanLy.ChiNhanh.Edit;QuanLyQuanAn.QuanLy.BaoCao.Access', 1),
(N'HeThong', N'QuanLyQuanAn.HeThong.PhanQuyen.Access;QuanLyQuanAn.HeThong.PhanQuyen.Edit;QuanLyQuanAn.HeThong.TaiKhoan.Access;QuanLyQuanAn.HeThong.TaiKhoan.Edit', 1)
;

INSERT INTO [dbo].[DanhMuc] ([TenDanhMuc], [MoTa])
VALUES
(N'Nước ngọt', N''),
(N'Bia', N''),
(N'Rượu', N''),
(N'Lẩu', N''),
(N'Phở', N''),
(N'Bún', N''),
(N'Các món gà', N'')
;

INSERT INTO [dbo].[MonAn] ([TenMonAn], [GiaThamKhao])
VALUES
(N'Trà đá', 5000),
(N'Lẩu hải sản', 200000),
(N'Lẩu dê', 200000),
(N'Phở bò', 200000),
(N'Phở gà', 200000),
(N'Gà luộc', 200000),
(N'Gà chiên nước mắm', 200000),
(N'Đùi gà nướng', 200000),
(N'Gà rô-ti', 200000),
(N'Chân gà nướng', 200000),
(N'Cánh gà nướng', 200000),
(N'Gà quay nguyên con', 200000),
(N'Gà xé lá chanh', 200000),
(N'Trứng gà luộc', 200000),
(N'Trứng gà nướng', 200000)
;

INSERT INTO [dbo].[ChiNhanh] ([TenChiNhanh], [DiaChi], [SoDienThoai])
VALUES
(N'Chi nhánh Quận 1', N'135B Trần Hưng Đạo, P. Cầu Ông Lãnh, Quận 1, Tp. HCM', N'0123456789'),
(N'Chi nhánh Quận 5', N'227 Nguyễn Văn Cừ, P. 4, Quận 5, Tp. HCM', N'0123456789')
;
